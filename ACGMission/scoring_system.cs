﻿using System.Text;
using System;
using System.Collections;
using maddox.game;
using maddox.game.world;
using maddox.GP;
using part;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;

namespace ACGMission
{
    public class ScoringSystem
    {
        private BaseMission _mission;
        private Dictionary<int, BombTarget> _bombTargets;
        private int _bombTargetID;
        private Dictionary<String, bool> _landedAircraft = new Dictionary<String, bool>();

        private Dictionary<String, bool> _scoredActors = new Dictionary<String, bool>();

        public int KillScoreRed { get; private set; }
        public int KillScoreBlue { get; private set; }

        public int GroundKillScoreRed { get; private set; }
        public int GroundKillScoreBlue { get; private set; }

        public int BombScoreRed { get; private set; }
        public int BombScoreBlue { get; private set; }

        public int OverallScoreRed() { return KillScoreRed + GroundKillScoreRed + BombScoreRed + AdditionalScoreRed; }
        public int OverallScoreBlue() { return KillScoreBlue + GroundKillScoreBlue + BombScoreBlue + AdditionalScoreBlue; }

        public int KillScoreMultiplicatorRed { get; set; }
        public int KillScoreMultiplicatorBlue { get; set; }

        public int GroundKillScoreMultiplicatorRed { get; set; }
        public int GroundKillScoreMultiplicatorBlue { get; set; }

        public int BombScoreMultiplicatorRed { get; set; }
        public int BombScoreMultiplicatorBlue { get; set; }

        public int AdditionalScoreRed { get; set; }
        public int AdditionalScoreBlue { get; set; }

        public bool CountCrashLandingsInHomeTerritoryAsKills { get; set; }
        public bool InformAboutGroundKills { get; set; }

        public ScoringSystem(BaseMission mission)
        {
            _mission = mission;
            _bombTargets = new Dictionary<int, BombTarget>();
            _bombTargetID = 0;

            KillScoreRed = 0;
            KillScoreBlue = 0;
            GroundKillScoreRed = 0;
            GroundKillScoreBlue = 0;
            BombScoreRed = 0;
            BombScoreBlue = 0;
            AdditionalScoreRed = 0;
            AdditionalScoreBlue = 0;

            KillScoreMultiplicatorRed = 1;
            KillScoreMultiplicatorBlue = 1;
            GroundKillScoreMultiplicatorRed = 1;
            GroundKillScoreMultiplicatorBlue = 1;
            BombScoreMultiplicatorRed = 1;
            BombScoreMultiplicatorBlue = 1;

            CountCrashLandingsInHomeTerritoryAsKills = true;
            InformAboutGroundKills = true;
        }

        public int AddBombTarget(BombTarget bombTarget)
        {
            int id = _bombTargetID++;
            _bombTargets.Add(id, bombTarget);
            return id;
        }

        public void RemoveBombTarget(int id)
        {
            if (!_bombTargets.ContainsKey(id))
                return;

            _bombTargets.Remove(id);
        }

        public void AircraftLanded(AiAircraft aircraft, bool isCrashLanding)
        {
            String id = AircraftID(aircraft);
            if (!_landedAircraft.ContainsKey(id))
            {
                _landedAircraft.Add(id, true);
                if (!_mission.IsAircraftInHomeTerritroy(aircraft) || (isCrashLanding && CountCrashLandingsInHomeTerritoryAsKills))
                {
                    IncreaseKillScoreIfActorIsDamagedByEnemy(aircraft);
                    _mission._eventOutHandler.LogAircraftLandedScore(_mission, _mission.GetGameTime().ToString("HH:mm:ss"), aircraft, isCrashLanding);
                }
            }
        }

        public void ActorDead(AiActor actor, string DeadDestroyed)
        {
            string id = actor.Name();
            if (actor is AiAircraft)
            {
                id = AircraftID(actor as AiAircraft);
            }

            if (_scoredActors.ContainsKey(id) == false)
            {
                _scoredActors.Add(id, true);
                if (actor is AiAircraft)
                {
                    AiAircraft aircraft = (actor as AiAircraft);
                    if (!_landedAircraft.ContainsKey(id))
                    {
                        IncreaseKillScoreIfActorIsDamagedByEnemy(aircraft);
                        _mission._eventOutHandler.LogActorDeadDestroyed(_mission, _mission.GetGameTime().ToString("HH:mm:ss"), actor, DeadDestroyed);
                    }
                }
                else if (actor is AiGroundActor)
                {
                    //Is it in a valid target zone?
                    bool deathCounts = false;
                    foreach (BombTarget bt in _bombTargets.Values)
                    {

                        if (bt.IsHit(actor.Pos().x, actor.Pos().y))
                        {
                            if (bt.Army != (Army)actor.Army())
                            {
                                //We can count it
                                deathCounts = true;
                            }
                        }
                    }

                    if (IsActorDamagedByEnemy(actor))
                    {
                        bool logged = false;
                        if (deathCounts == true)
                        {
                            IncreaseGroundKillScoreIfActorIsDamagedByEnemy(actor);
                            _mission._eventOutHandler.LogActorDeadDestroyed(_mission, _mission.GetGameTime().ToString("HH:mm:ss"), actor, DeadDestroyed);
                            logged = true;
                        }

                        AiGroundActor g = (actor as AiGroundActor);
                        if (logged == false)
                        {
                            if ((int)g.Type() >= 30)
                            {
                                if ((int)g.Type() < 38)
                                {
                                    _mission._eventOutHandler.LogActorDeadDestroyed(_mission, _mission.GetGameTime().ToString("HH:mm:ss"), actor, DeadDestroyed);
                                }
                            }
                        }
                    }
                }
            } 
        }

        private void IncreaseKillScoreIfActorIsDamagedByEnemy(AiActor actor)
        {
            if (!IsActorDamagedByEnemy(actor))
                return;

            Army army = (Army)actor.Army();

            if (army == Army.Blue)
                IncreaseKillScoreRed();
            else if (army == Army.Red)
                IncreaseKillScoreBlue();
        }

        private void IncreaseKillScoreRed()
        {
            KillScoreRed += (1 * KillScoreMultiplicatorRed);
        }

        private void IncreaseKillScoreBlue()
        {
            KillScoreBlue += (1 * KillScoreMultiplicatorBlue);
        }

        private void IncreaseGroundKillScoreIfActorIsDamagedByEnemy(AiActor actor)
        {
            if (!IsActorDamagedByEnemy(actor))
                return;

            Army army = (Army)actor.Army();

            if (army == Army.Blue)
                IncreaseGroundKillScoreRed();
            else if (army == Army.Red)
                IncreaseGroundKillScoreBlue();

            if (InformAboutGroundKills)
            {
                string name = "Ground unit";
                AiGroundGroup groundGroup = (AiGroundGroup)actor.Group();
                if (groundGroup != null)
                {
                    if (groundGroup.GroupType() == AiGroundGroupType.Ship)
                        name = "Ship";
                    else if (groundGroup.GroupType() == AiGroundGroupType.Train)
                        name = "Train";
                }

                _mission.Message(To.Hud, String.Format("{0} was destroyed in map grid {1}!", name, _mission.GamePlay.gpSectorName(actor.Pos().x, actor.Pos().y)));
            }
        }

        private void IncreaseGroundKillScoreRed()
        {
            GroundKillScoreRed += (1 * GroundKillScoreMultiplicatorRed);
        }

        private void IncreaseGroundKillScoreBlue()
        {
            GroundKillScoreBlue += (1 * GroundKillScoreMultiplicatorBlue);
        }

        private void IncreaseBombScoreRed()
        {
            BombScoreRed += (1 * BombScoreMultiplicatorRed);
        }

        private void IncreaseBombScoreBlue()
        {
            BombScoreBlue += (1 * BombScoreMultiplicatorBlue);
        }

        private bool IsActorDamagedByEnemy(AiActor actor)
        {
            ArrayList damageInitiators = _mission.Battle.GetDamageInitiators(actor);
            foreach (DamagerScore di in damageInitiators)
            {
                if (di.initiator.Actor.Army() != actor.Army())
                    return true;
            }
            return false;
        }

        public bool BombExplosion(double mass, Point3d pos, AiDamageInitiator initiator)
        {
            bool targetWasHit = false;

            AiAircraft aircraft = (initiator.Actor as AiAircraft);
            if (aircraft != null)
            {
                Army army = (Army)initiator.Actor.Army();
                foreach (BombTarget bt in _bombTargets.Values)
                {

                    if (bt.Army == army && 
                        bt.IsHit(pos.x, pos.y))
                    {
                        targetWasHit = true;

                        string aircraftId = AircraftID(aircraft);
                        if (!bt.Scorers.Contains(aircraftId))
                        {
                            bt.Scorers.Add(aircraftId);

                            if (army == Army.Blue)
                                IncreaseBombScoreBlue();
                            else if (army == Army.Red)
                                IncreaseBombScoreRed();
                        }

                        if (bt.InformAboutHits)
                            _mission.Message(To.Hud, bt.Name + " is under attack!");

                        if (bt.SpawnSmoke)
                        {
                            Random random = new Random();
                            if (pos.z > 0 && // no smoke on water
                                random.Next(1, 3) == 2) // 33.3% chance to spawn a smoke plum
                            {
                                _mission.SpawnRandomSmoke(pos);
                            }
                        }

                        break;
                    }
                }
            }

            return targetWasHit;
        }

        private String AircraftID(AiAircraft a)
        {
            // 3:BoB_LW_JG26_II.000:1
            // 2:BoB_RAF_F_222Sqn_Early.210:11
            return a.Name() + ":" + a.HullNumber();
        }


        public class BombTarget
        {
            public string Name { get; set; }
            public Army Army { get; set; }
            public bool SpawnSmoke { get; set; }
            public bool InformAboutHits { get; set; }

            public int PolySides { get; set; }
            public double[] PolyX { get; set; }
            public double[] PolyY { get; set; }

            public List<String> Scorers { get; set; }

            public BombTarget()
            {
                Scorers = new List<string>();
            }

            public BombTarget(string name, Army army, bool spawnSmoke, bool informAboutHits, int polySides, double[] polyX, double[] polyY)
            {
                this.Name = name;
                this.Army = army;
                this.SpawnSmoke = spawnSmoke;
                this.InformAboutHits = informAboutHits;
                this.PolySides = polySides;
                this.PolyX = polyX;
                this.PolyY = polyY;

                Scorers = new List<string>();
            }

            public bool IsHit(double x, double y)
            {
                bool result = false;
                int i, j = PolySides - 1;

                for (i = 0; i < PolySides; i++)
                {
                    if ((PolyY[i] < y && PolyY[j] >= y
                    || PolyY[j] < y && PolyY[i] >= y)
                    && (PolyX[i] <= x || PolyX[j] <= x))
                    {
                        result ^= (PolyX[i] + (y - PolyY[i]) / (PolyY[j] - PolyY[i]) * (PolyX[j] - PolyX[i]) < x);
                    }
                    j = i;
                }

                return result;
            }
        }
    }
}