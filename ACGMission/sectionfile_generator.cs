﻿using System;
using System.IO;
using System.Collections;
using maddox.game;
using maddox.game.world;
using maddox.GP;
using part;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;

namespace ACGMission
{
    public class SectionFileGenerator
    {
        public static void createSectionFileFromString( ISectionFile sf, string missionString )
        {
            StringReader strReader = new StringReader(missionString);

            string currentSection = "";
            string line = strReader.ReadLine();
            while (line != null)
            {
                line = line.Trim();

                if (line != "")
                {
                    if (line.StartsWith("[") && line.EndsWith("]"))
                    {
                        currentSection = line.Substring(1, line.Length - 2);
                    }
                    else
                    {
                        int spacePos = line.IndexOf(" ");

                        string key = "";
                        string value = "";
                        if (spacePos == -1)
                        {
                            key = line;
                        }
                        else
                        {
                            key = line.Substring(0, spacePos);
                            value = line.Substring(spacePos + 1);
                        }

                        sf.add(currentSection, key, value);
                    }
                }

                line = strReader.ReadLine();
            }
        }
    }
}