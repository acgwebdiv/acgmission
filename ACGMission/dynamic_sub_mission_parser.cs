﻿using System;
using System.Collections.Generic;
using System.IO;
using maddox.game;

namespace ACGMission
{
    public class DynamicSubMissionParser
    {
        private BaseMission _mission;

        public DynamicSubMissionParser(BaseMission mission)
        {
            _mission = mission;
        }

        public DynamicSubMission parseFile(string filePath)
        {
            DynamicSubMission result = new DynamicSubMission();
            string lineBreak = Environment.NewLine;

            try
            {
                FileInfo fi = new FileInfo(filePath);
                result.FileName = fi.Name.Split('.')[0];

                string title = "";
                string exclusive = "";
                string duration = "";
                string briefingRed = "";
                string briefingBlue = "";
                string misCommon = "";
                string misAiBombersRed = "";
                string misAiBombersBlue = "";
                string misAiFightersRed = "";
                string misAiFightersBlue = "";
                string bombTargetsRed = "";
                string bombTargetsBlue = "";

                using (StreamReader sr = new StreamReader(filePath))
                {
                    string currentTag = null;
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (line.StartsWith("###"))
                        {
                            currentTag = line.Substring(3);
                        }
                        else if( currentTag != null)
                        {
                            string lineWithBreak = line + lineBreak;

                            switch (currentTag)
                            {
                                case "Title":
                                    title += line;
                                    break;
                                case "Exclusive":
                                    exclusive += line;
                                    break;
                                case "Duration":
                                    duration += line;
                                    break;
                                case "BriefingRed":
                                    briefingRed += lineWithBreak;
                                    break;
                                case "BriefingBlue":
                                    briefingBlue += lineWithBreak;
                                    break;
                                case "Mis-Common":
                                    misCommon += lineWithBreak;
                                    break;
                                case "Mis-AiBombers-Red":
                                    misAiBombersRed += lineWithBreak;
                                    break;
                                case "Mis-AiBombers-Blue":
                                    misAiBombersBlue += lineWithBreak;
                                    break;
                                case "Mis-AiFighters-Red":
                                    misAiFightersRed += lineWithBreak;
                                    break;
                                case "Mis-AiFighters-Blue":
                                    misAiFightersBlue += lineWithBreak;
                                    break;
                                case "Bomb-Targets-Red":
                                    bombTargetsRed += lineWithBreak;
                                    break;
                                case "Bomb-Targets-Blue":
                                    bombTargetsBlue += lineWithBreak;
                                    break;
                            }
                        }
                    }
                }

                SetTitle(result, title);
                SetExclusive(result, exclusive);
                SetDuration(result, duration);
                SetBriefingRed(result, briefingRed);
                SetBriefingBlue(result, briefingBlue);
                SetMisCommon(result, misCommon);
                SetMisAiBombersRed(result, misAiBombersRed);
                SetMisAiBombersBlue(result, misAiBombersBlue);
                SetMisAiFightersRed(result, misAiFightersRed);
                SetMisAiFightersBlue(result, misAiFightersBlue);
                SetBombTargetsRed(result, bombTargetsRed);
                SetBombTargetsBlue(result, bombTargetsBlue);
            }
            catch (Exception)
            {
            }

            return result;
        }

        private void SetTitle(DynamicSubMission dsm, string s)
        {
            dsm.Title = s;
        }

        private void SetExclusive(DynamicSubMission dsm, string s)
        {
            bool exclusive = false;
            string sCorrected = s.ToLower();

            if (sCorrected == "true" || sCorrected == "yes" || sCorrected == "1")
                exclusive = true;

            dsm.Exclusive = exclusive;
        }

        private void SetDuration(DynamicSubMission dsm, string s)
        {
            dsm.Duration = Int32.Parse(s);
        }

        private void SetBriefingRed(DynamicSubMission dsm, string s)
        {
            dsm.BriefingRed = s;
        }

        private void SetBriefingBlue(DynamicSubMission dsm, string s)
        {
            dsm.BriefingBlue = s;
        }

        private void SetMisCommon(DynamicSubMission dsm, string s)
        {
            dsm.MissionCommon = CreateSectionFile(s);
        }

        private void SetMisAiBombersRed(DynamicSubMission dsm, string s)
        {
            dsm.MissionAiBombersRed = CreateSectionFile(s);
        }

        private void SetMisAiBombersBlue(DynamicSubMission dsm, string s)
        {
            dsm.MissionAiBombersBlue = CreateSectionFile(s);
        }

        private void SetMisAiFightersRed(DynamicSubMission dsm, string s)
        {
            dsm.MissionAiFightersRed = CreateSectionFile(s);
        }

        private void SetMisAiFightersBlue(DynamicSubMission dsm, string s)
        {
            dsm.MissionAiFightersBlue = CreateSectionFile(s);
        }

        private void SetBombTargetsRed(DynamicSubMission dsm, string s)
        {
            dsm.BombTargetsRed = CreateBombTargets(s, Army.Red);
        }

        private void SetBombTargetsBlue(DynamicSubMission dsm, string s)
        {
            dsm.BombTargetsBlue = CreateBombTargets(s, Army.Blue);
        }

        private ISectionFile CreateSectionFile(string s)
        {
            if (s.Length == 0)
                return null;

            ISectionFile sf = _mission.GamePlay.gpCreateSectionFile();
            SectionFileGenerator.createSectionFileFromString(sf, s);
            return sf;
        }

        private List<ScoringSystem.BombTarget> CreateBombTargets(string s, Army army)
        {
            if (s.Length == 0)
                return null;

            List<ScoringSystem.BombTarget> result = new List<ScoringSystem.BombTarget>();

            StringReader strReader = new StringReader(s);

            ScoringSystem.BombTarget bombTarget = null;

            string line = strReader.ReadLine();
            while (line != null)
            {
                line = line.Trim();

                if (line != "")
                {
                    if (line.StartsWith("[") && line.EndsWith("]"))
                    {
                        if (bombTarget != null)
                            result.Add(bombTarget);

                        bombTarget = new ScoringSystem.BombTarget();
                        bombTarget.Name = line.Substring(1, line.Length - 2);
                        bombTarget.Army = army;
                    }
                    else if (bombTarget != null)
                    {

                        if (line == "Coordinates")
                        {
                            List<double> xList = new List<double>();
                            List<double> yList = new List<double>();

                            bool ok = true;
                            line = strReader.ReadLine();
                            while (ok && line != null)
                            {
                                string[] arr = line.Split(' ');
                                if (arr.Length == 2)
                                {
                                    double x = 0.0;
                                    double y = 0.0;

                                    ok = ok && Double.TryParse(arr[0], out x);
                                    ok = ok && Double.TryParse(arr[1], out y);

                                    if (ok)
                                    {
                                        xList.Add(x);
                                        yList.Add(y);
                                        line = strReader.ReadLine();
                                    }
                                }
                                else
                                    ok = false;
                            }

                            bombTarget.PolyX = xList.ToArray();
                            bombTarget.PolyY = yList.ToArray();
                            bombTarget.PolySides = xList.Count;

                            continue;
                        }
                        else
                        {
                            int spacePos = line.IndexOf(" ");
                            string key = line.Substring(0, spacePos);
                            string value = line.Substring(spacePos + 1);

                            switch (key)
                            {
                                case "BombKgToPointRatio":
                                    // Not in use anymore
                                    break;
                                case "SpawnSmoke":
                                    bombTarget.SpawnSmoke = value == "1" ? true : false;
                                    break;
                                case "InformAboutHits":
                                    bombTarget.InformAboutHits = value == "1" ? true : false;
                                    break;
                            }
                        }
                    }
                }

                line = strReader.ReadLine();
            }

            if (bombTarget != null)
                result.Add(bombTarget);


            return result;
        }
    }
}