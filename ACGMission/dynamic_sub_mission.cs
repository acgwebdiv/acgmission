﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using maddox.game;
using maddox.game.world;
using maddox.GP;
using part;

namespace ACGMission
{
    public class DynamicSubMission
    {
        public string FileName { get; set; }

        public string Title { get; set; }
        public bool Exclusive { get; set; }
        public int Duration { get; set; }
        public string BriefingRed { get; set; }
        public string BriefingBlue { get; set; }
        public ISectionFile MissionCommon { get; set; }
        public ISectionFile MissionAiBombersRed { get; set; }
        public ISectionFile MissionAiBombersBlue { get; set; }
        public ISectionFile MissionAiFightersRed { get; set; }
        public ISectionFile MissionAiFightersBlue { get; set; }
        public List<ScoringSystem.BombTarget> BombTargetsRed { get; set; }
        public List<ScoringSystem.BombTarget> BombTargetsBlue { get; set; }

        public int RunCount { get; set; }
        public DateTime StartTime { get; private set; }
        public List<int> BombTargetIDs { get; private set; }
        private Stopwatch _runningStopWatch;

        public DynamicSubMission()
        {
            StartTime = default(DateTime);
            BombTargetIDs = new List<int>();
            _runningStopWatch = new Stopwatch();
        }

        public void StartRunning(DateTime startTime, List<int> bombTargetIDs )
        {
            StartTime = startTime;
            RunCount += 1;
            BombTargetIDs = bombTargetIDs;
            _runningStopWatch.Start();
        }

        public void StopRunning()
        {
            _runningStopWatch.Reset();
            StartTime = default(DateTime);
            BombTargetIDs.Clear();
        }

        public bool IsRunning()
        {
            return _runningStopWatch.IsRunning;
        }

        public bool IsFinished()
        {
            if (_runningStopWatch.Elapsed.TotalMinutes >= Duration)            
                return true;

            return false;
        }

        public string GetBriefing(Army army)
        {
            string briefing = null;

            if (army == Army.Red)
                briefing = BriefingRed;
            else if (army == Army.Blue)
                briefing = BriefingBlue;

            if (briefing == null)
                return briefing;

            string placeHolder = "";
            int placeHolderStart = -1;
            for (int i = 0; i < briefing.Length; i++)
            {
                char c = briefing[i];

                if (placeHolderStart == -1 && c == '{')
                {
                    placeHolderStart = i;
                    continue;
                }

                if (placeHolderStart > -1)
                {
                    if (c == '}')
                    {
                        string text = ReplaceBriefingPlaceHolder(placeHolder);
                        briefing = briefing.Remove(placeHolderStart, placeHolder.Length + 2);
                        briefing = briefing.Insert(placeHolderStart, text);
                        i = placeHolderStart + text.Length;

                        placeHolderStart = -1;
                        placeHolder = "";
                    }
                    else
                    {
                        placeHolder += c;
                    }
                }
            }
                

            return briefing;
        }

        private string ReplaceBriefingPlaceHolder(string placeHolder)
        {
            string[] arr = placeHolder.Split(':');

            if (arr.Length != 2)
                return placeHolder;

            string key = arr[0].ToLower();
            string value = arr[1];
            string result = "";

            switch (key)
            {
                case "time":
                    int minutes = 0;
                    Int32.TryParse(value, out minutes);
                    result = StartTime.AddMinutes(minutes).ToString("hh:mm");
                    break;
            }

            return result;
        }
    }
}