﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACGMission
{
    public enum Army 
    {
        Any = -1,
        None = 0,
        Red = 1, 
        Blue = 2
    }

#region MessageEnmus
    [Flags]
    public enum To
    { 
        Hud = 1, 
        Log = 2, 
        HudAndLog = Hud | Log 
    }

    public enum For
    { 
        All = -1, 
        Any = All, 
        Unknown = 0, 
        Red = 1, 
        Blue = 2, 
        England = Red, 
        German = Blue 

    }
#endregion
}
