﻿using System.Text;
using System;
using System.Collections;
using maddox.game;
using maddox.game.world;
using maddox.GP;
using part;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;

namespace ACGMission
{
    public class XMission : AMission
    {
        private const int TimeTick = 1;
        private const int TimeSecond = TimeTick * 30;
        private const int TimeMinute = TimeSecond * 60;
        private System.Collections.Generic.List<XInfo> _extensions;
        private System.Collections.Generic.List<XInfo> _disabledExtensions;
        private bool _isInitialized;
        private bool _isLogEnabled;
        private bool _il2xEnabled = false;

        private bool IsSinglePlayerMode { get { return !IsOnlineMode; } }
        private bool IsOnlineMode { get { return GamePlay.gpIsServerDedicated(); } }

        public string docFolderPath { get; set; }

        private class XInfo
        {
            public readonly object Instance;
            public readonly System.Collections.Generic.Dictionary<string, System.Reflection.MethodInfo> Methods;
            public bool IsRunning { get; set; }

            public XInfo(object instance, System.Collections.Generic.Dictionary<string, System.Reflection.MethodInfo> methods)
            {
                Instance = instance;
                Methods = methods;
            }
        }

        public bool IsLogEnabled
        {
            get { return _isLogEnabled && _isInitialized; }
            set { _isLogEnabled = value; }
        }

        private static void configureExtension(object x, string path)
        {
            var mi = x.GetType().GetMethod("ConfigureFromFile");
            mi.Invoke(x, new object[] { path });
        }

        private const string LogExtensionSection = "\n..........................................................\n";

        public static bool IsIL2XStarted { get; set; }

        protected bool RunIL2X(string directory = null)
        {
            _il2xEnabled = true;
            const string LogSectionLineBegin = "\n|||||||||||||||||||||||||||||||| IL2X begins ||||||||||||||||||||||||||||||||\n";
            const string LogSectionLineEnd = "\n||||||||||||||||||||||||||||||||| IL2X ends |||||||||||||||||||||||||||||||||\n";

            if (IsIL2XStarted)
            {
                Log("{0}Skips IL2X initialization (system is already running) ...", LogSectionLineBegin);
                Log(LogSectionLineEnd);
                return false;
            }

            MissionNumberListener = -1;
            Log("{0}Loads all installed extensions ...", LogSectionLineBegin);
            if (string.IsNullOrEmpty(directory))
                directory = IL2XRootFolder;
            if (!System.IO.Directory.Exists(directory))
            {
                Log("ERROR! Extensions directory not found: {0}\n{1}", directory, LogSectionLineEnd);
                return false;
            }
            var filenames = System.IO.Directory.GetFiles(directory, "*.il2x");
            if (filenames.Length == 0)
            {
                Log("No extensions found in directory: {0}{1}", directory, LogSectionLineEnd);
                return false;
            }
            foreach (var xFilename in filenames)
            {
                var xName = System.IO.Path.GetFileNameWithoutExtension(xFilename);
                Log("{0}------- {1} -------", LogExtensionSection, System.IO.Path.GetFileName(xName));
                var x = loadExtensionFromFile(System.IO.Path.Combine(directory, xFilename));
                if (x != null)
                {
                    StartExtension(x);
                    Log("{0} is now running.", x);
                }
                Log(LogExtensionSection);
            }
            //invokeExtensions("OnSystemReady");
            Log(LogSectionLineEnd);
            return true;
        }

        private object loadExtensionFromFile(string path)
        {
            try
            {
                var xName = System.IO.Path.GetFileNameWithoutExtension(path);
                var configuration = loadExtensionConfigurationFromFile(path);
                if (isIgnored(configuration))
                {
                    Log("Extension {0} is currently ignored and will not be loaded", xName);
                    return null;
                }
                if (!configuration.ContainsKey("Path"))
                {
                    Log("ERROR! Configuration file {0} is missing a crucial value: Path=<path to extension assembly>", path);
                    return null;
                }
                if (!configuration.ContainsKey("Type"))
                {
                    Log("ERROR! Configuration file {0} is missing a mandatory value: Type=<extension .NET Type>", path);
                    return null;
                }
                Log("Successfully loaded extension configuration from {0}", path);

                var x = loadExtension(false, configuration["Type"], configuration["Path"]);
                configureExtension(x, path);
                return x;
            }
            catch (Exception ex)
            {
                Log("ERROR! Unhandled exception while loading extensions from file {0}. {1}", path, ex.ToString());
                throw;
            }
        }

        private static bool isIgnored(System.Collections.Generic.Dictionary<string, string> configuration)
        {
            foreach (var kvp in configuration)
            {
                if (!"IsIgnored".Equals(kvp.Key, StringComparison.OrdinalIgnoreCase))
                    continue;

                var value = kvp.Value.ToLowerInvariant();
                return "true".Equals(value) || "yes".Equals(value) || "1".Equals(value);
            }
            return false;
        }

        private System.Collections.Generic.Dictionary<string, string> loadExtensionConfigurationFromFile(string path)
        {
            var result = new System.Collections.Generic.Dictionary<string, string>();
            using (var stream = new System.IO.FileStream(path, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read))
            using (var reader = new System.IO.StreamReader(stream))
            {
                do
                {
                    var line = reader.ReadLine();
                    if (line == null)
                        break;
                    if (line.Length == 0)
                        continue;
                    var kvp = parseConfigurationLine(line);
                    if (kvp.HasValue)
                        result[((System.Collections.Generic.KeyValuePair<string, string>)kvp).Key] = ((System.Collections.Generic.KeyValuePair<string, string>)kvp).Value;
                }
                while (true);
            }
            return result;
        }

        private System.Collections.Generic.KeyValuePair<string, string>? parseConfigurationLine(string s)
        {
            var line = s.Trim();
            if (line.StartsWith(";"))
                return null; // line is a comment

            var split = line.Split(new[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
            if (split.Length != 2)
            {
                Log("ERROR! Invalid configuration found: '{0}'", line);
                return null;
            }
            return new System.Collections.Generic.KeyValuePair<string, string>(split[0], split[1]);
        }

        private object loadExtension(bool start, string xName, string assemblyPath, params object[] args)
        {
            assemblyPath = resolvePath(assemblyPath);
            if (!System.IO.File.Exists(assemblyPath))
            {
                Log("Cannot load extension from {0}. Assembly file not found.", assemblyPath);
                return null;
            }
            Log("Adds extension from: {0} ...", assemblyPath);

            AppDomain.CurrentDomain.AssemblyResolve += (sender, e) =>
            {
                Log("{0}DEBUG: Resolves assembly '{1}' ...", LogIndent1, e.Name);
                return probeForAssembly(e.Name, IL2XRootFolder, true);
            };
            System.Reflection.Assembly asm;
            try
            {
                asm = AppDomain.CurrentDomain.Load(assemblyPath);
            }
            catch (Exception)
            {
                Log("FAILURE: Failed to load library from {0}", assemblyPath);
                return null;
            }
            Log("Assembly full name: {0}", asm.FullName);
            var types = asm.GetTypes();
            Type xType = null;
            System.Collections.Generic.Dictionary<string, System.Reflection.MethodInfo> reflection = null;
            foreach (var type in types)
            {
                Log("{0}Type '{1}' found.", LogIndent1, type);
                if (!xName.Equals(type.Name) && !xName.Equals(type.FullName)) continue;
                Log("{0}Extension '{1}' found.", LogIndent1, type);
                xType = type;
                reflection = loadMissionExtensionReflection(xType);
                break;
            }
            if (reflection == null)
            {
                Log("** FAILURE: No extension found with name '{0}'", xName);
                return null;
            }

            var x = Activator.CreateInstance(xType, args);
            _extensions = _extensions ?? new System.Collections.Generic.List<XInfo>();
            _extensions.Add(new XInfo(x, reflection));
            if (start)
            {
                Log("Starts extension ...");
                StartExtension(x);
            }
            Log("SUCCESS! (extension is now loaded into sim)",
                _extensions.Count,
                start ? " and running" : string.Empty);
            return x;
        }

        private const string LogIndent1 = "    ";

        private System.Reflection.Assembly probeForAssembly(string assemblyId, string folder, bool probeSubFolders)
        {
            // note: The 'assemblyId' might be a file path or a fully Assembly (full) Name ...
            //       This algorithm always assumes the assembly short name is also the name of the assembly file
            //       suffixed by '.dll'. A more advanced algorithm would load all assemblies it found in a different
            //       "sandbox" AppDomain and check the assemby name.
            Log("{0}Probes folder {1} ...", LogIndent1, folder);
            var cutAt = assemblyId.IndexOf(", Version", StringComparison.Ordinal);
            var fileName = cutAt != -1 ? assemblyId.Substring(0, cutAt) : System.IO.Path.GetFileName(assemblyId);
            if (!fileName.EndsWith(".dll", StringComparison.OrdinalIgnoreCase))
                fileName = string.Concat(fileName, ".dll");
            if (string.IsNullOrEmpty(fileName))
                return null;

            var testPath = System.IO.Path.Combine(folder, fileName);
            Log("{0}Tests path {1} ...", LogIndent1, testPath);
            if (System.IO.File.Exists(testPath))
            {
                try
                {
                    var asm = System.Reflection.Assembly.LoadFile(testPath);
                    if (asm != null)
                    {
                        Log("{0}SUCCESS! Assembly found at: {1}", LogIndent1, testPath);
                        return asm;
                    }
                }
                catch (Exception ex)
                {
                    Log(string.Format("{0}ERROR! Unhandled error while loading assembly file: {0}", ex), LogIndent1);
                }
            }

            if (!probeSubFolders)
                return null;

            Log("{0}Probes sub folders ...", LogIndent1);
            foreach (var subFolder in System.IO.Directory.GetDirectories(folder))
            {
                var asm = probeForAssembly(assemblyId, subFolder, true);
                if (asm != null)
                    return asm;
            }
            return null;
        }

        private string resolvePath(string assemblyPath)
        {
            if (isPathAbsolute(assemblyPath))
                return assemblyPath;

            return System.IO.Path.Combine(IL2XRootFolder, "bin", assemblyPath.StartsWith(".\\")
                ? assemblyPath.Substring(2)
                : assemblyPath);
        }

        private static bool isPathAbsolute(string path)
        {
            return path.StartsWith("\\") || (path.Length >= 3 && path[1] == ':' && path[2] == '\\');
        }

        protected void StartExtension(object x)
        {
            var xInfo = findExtensionInfo(x);
            xInfo.IsRunning = true;
            var mi = x.GetType().GetMethod("Start");
            mi.Invoke(x, new object[] { this });
        }

        protected void StopExtension(object x)
        {
            var xInfo = findExtensionInfo(x);
            xInfo.IsRunning = false;
            var mi = x.GetType().GetMethod("Stop");
            mi.Invoke(x, new object[] { this });
        }

        private XInfo findExtensionInfo(object x)
        {
            foreach (var xInfo in _extensions)
            {
                if (xInfo.Instance == x)
                    return xInfo;
            }
            return null;
        }

        #region .  Extension Interface  .

        private string _il2XRootFolder;

        public string IL2XRootFolder
        {
            get { return _il2XRootFolder; }
            set
            {
                if (value.Equals(_il2XRootFolder)) return;
                _il2XRootFolder = value;
            }
        }

        private static System.Collections.Generic.Dictionary<string, System.Reflection.MethodInfo> loadMissionExtensionReflection(Type xType)
        {
            var result = new System.Collections.Generic.Dictionary<string, System.Reflection.MethodInfo>();
            result["Init"] = xType.GetMethod("Init");
            result["OnTickGame"] = xType.GetMethod("OnTickGame");
            result["OnTickReal"] = xType.GetMethod("OnTickReal");
            result["OnBattleInit"] = xType.GetMethod("OnBattleInit");
            result["OnBattleStarted"] = xType.GetMethod("OnBattleStarted");
            result["OnBattleStoped"] = xType.GetMethod("OnBattleStoped");
            result["OnMissionLoaded"] = xType.GetMethod("OnMissionLoaded");
            result["OnPlayerConnected"] = xType.GetMethod("OnPlayerConnected");
            result["OnPlayerDisconnected"] = xType.GetMethod("OnPlayerDisconnected");
            result["OnPlayerArmy"] = xType.GetMethod("OnPlayerArmy");
            result["OnActorCreated"] = xType.GetMethod("OnActorCreated");
            result["OnActorDestroyed"] = xType.GetMethod("OnActorDestroyed");
            result["OnActorDamaged"] = xType.GetMethod("OnActorDamaged");
            result["OnActorDead"] = xType.GetMethod("OnActorDead");
            result["OnActorTaskCompleted"] = xType.GetMethod("OnActorTaskCompleted");
            result["OnTrigger"] = xType.GetMethod("OnTrigger");
            result["OnAircraftDamaged"] = xType.GetMethod("OnAircraftDamaged");
            result["OnPlaceEnter"] = xType.GetMethod("OnPlaceEnter");
            result["OnPersonParachuteFailed"] = xType.GetMethod("OnPersonParachuteFailed");
            result["OnPersonParachuteLanded"] = xType.GetMethod("OnPersonParachuteLanded");
            result["OnPersonHealth"] = xType.GetMethod("OnPersonHealth");
            result["OnPersonMoved"] = xType.GetMethod("OnPersonMoved");
            result["OnAircraftKilled"] = xType.GetMethod("OnAircraftKilled");
            result["OnAircraftCrashLanded"] = xType.GetMethod("OnAircraftCrashLanded");
            result["OnAircraftLanded"] = xType.GetMethod("OnAircraftLanded");
            result["OnAircraftTookOff"] = xType.GetMethod("OnAircraftTookOff");
            result["OnAircraftCutLimb"] = xType.GetMethod("OnAircraftCutLimb");
            result["OnAircraftLimbDamaged"] = xType.GetMethod("OnAircraftLimbDamaged");
            result["OnOrderMissionMenuSelected"] = xType.GetMethod("OnOrderMissionMenuSelected");
            result["OnSingleBattleSuccess"] = xType.GetMethod("OnSingleBattleSuccess");
            result["OnAiAirNewEnemy"] = xType.GetMethod("OnAiAirNewEnemy");
            result["OnAutopilotOff"] = xType.GetMethod("OnAutopilotOff");
            result["OnAutopilotOn"] = xType.GetMethod("OnAutopilotOn");
            result["OnCarter"] = xType.GetMethod("OnCarter");
            result["OnPlaceLeave"] = xType.GetMethod("OnPlaceLeave");
            result["SendTimedHudMessage"] = xType.GetMethod("SendTimedHudMessage");
            result["SendTimedChatMessage"] = xType.GetMethod("SendTimedChatMessage");
            result["ProcessTimedMessages"] = xType.GetMethod("ProcessTimedMessages");
            result["IsMissionListener"] = xType.GetMethod("IsMissionListener");
            result["Timeout"] = xType.GetMethod("Timeout");
            result["OnUserDeleteUserLabel"] = xType.GetMethod("OnUserDeleteUserLabel");
            result["OnUserCreateUserLabel"] = xType.GetMethod("OnUserCreateUserLabel");
            result["OnBombExplosion"] = xType.GetMethod("OnBombExplosion");
            result["OnStationaryKilled"] = xType.GetMethod("OnStationaryKilled");
            result["OnBuildingKilled"] = xType.GetMethod("OnBuildingKilled");

            return result;
        }

        private void invokeExtensions(string methodName, params object[] args)
        {
            if (_extensions == null) return;
            var useArgs = prepareArgs(args);
            foreach (var xInfo in _extensions)
            {
                if (isExtensionDisabled(xInfo))
                    continue;
                try
                {
                    invokeExtension(xInfo, methodName, useArgs);
                }
                catch (Exception ex)
                {
                    disableExtension(xInfo);
                    writeCrashlog(ex);
                    Log("ERROR! While invoking method {0}.{1} (extension is now disabled):\n{2}", xInfo.Instance, methodName, ex.ToString());
                }
            }
        }

        private void writeCrashlog(Exception ex)
        {
            if (ex is System.Reflection.TargetInvocationException && ex.InnerException != null)
                ex = ex.InnerException;
            var x = getFirstRunningExtension();
            if (x != null)
                invokeExtension(x, "WriteCrashLog", ex, null);
        }

        private bool isExtensionDisabled(XInfo xInfo)
        {
            return _disabledExtensions != null && _disabledExtensions.Contains(xInfo);
        }

        private void disableExtension(XInfo xInfo)
        {
            _disabledExtensions = _disabledExtensions ?? new System.Collections.Generic.List<XInfo>();
            _disabledExtensions.Add(xInfo);
        }

        private object[] prepareArgs(object[] args)
        {
            var useArgs = new object[args != null ? args.Length + 1 : 1];
            useArgs[0] = this; // note: First argument is always (this) mission
            if (args != null)
                args.CopyTo(useArgs, 1);
            return useArgs;
        }

        private void invokeExtension(XInfo xInfo, string methodName, params object[] args)
        {
            var method = xInfo.Methods.ContainsKey(methodName)
                             ? xInfo.Methods[methodName]
                             : xInfo.Instance.GetType().GetMethod(methodName);
            if (method == null)
                Log("ERROR! Cannot invoke method '{0}' (not supported by XMission).");
            else
                method.Invoke(xInfo.Instance, args);
        }

        private XInfo getFirstRunningExtension()
        {
            if (_extensions == null) return null;
            foreach (var xInfo in _extensions)
            {
                if (xInfo.IsRunning)
                {
                    return xInfo;
                }
            }
            return null;
        }

        public override void Inited()
        {
            base.Inited();
            invokeExtensions("Inited");
            _isInitialized = true;
            Log("ExtensionRootFolder={0}", IL2XRootFolder);
        }

        public override void Init(ABattle battle, int missionNumber)
        {
            base.Init(battle, missionNumber);
            invokeExtensions("Init", battle, missionNumber);
        }

        public override void OnTickGame()
        {
            if (_il2xEnabled == true) {
                base.OnTickGame();
                invokeExtensions("OnTickGame");
                invokeExtension(getFirstRunningExtension(), "ProcessTimedMessages", prepareArgs(new object[0]));
            }
        }

        public override void OnTickReal()
        {
            base.OnTickReal();
            invokeExtensions("OnTickReal");
        }

        public override void OnBattleInit()
        {
            base.OnBattleInit();
            invokeExtensions("OnBattleInit");
        }

        public override void OnBattleStarted()
        {
            base.OnBattleStarted();
            invokeExtensions("OnBattleStarted");
        }

        public override void OnBattleStoped()
        {
            base.OnBattleStoped();
            invokeExtensions("OnBattleStoped");
        }

        public override void OnMissionLoaded(int missionNumber)
        {
            base.OnMissionLoaded(missionNumber);
            invokeExtensions("OnMissionLoaded", missionNumber);
        }

        public override void OnPlayerConnected(Player player)
        {
            base.OnPlayerConnected(player);
            invokeExtensions("OnPlayerConnected", player);
        }

        public override void OnPlayerDisconnected(Player player, string diagnostic)
        {
            base.OnPlayerDisconnected(player, diagnostic);
            invokeExtensions("OnPlayerDisconnected", player, diagnostic);
        }

        public override void OnPlayerArmy(Player player, int army)
        {
            base.OnPlayerArmy(player, army);
            invokeExtensions("OnPlayerArmy", player, army);
        }

        public override void OnActorCreated(int missionNumber, string shortName, AiActor actor)
        {
            base.OnActorCreated(missionNumber, shortName, actor);
            invokeExtensions("OnActorCreated", missionNumber, shortName, actor);
        }

        public override void OnActorDestroyed(int missionNumber, string shortName, AiActor actor)
        {
            base.OnActorDestroyed(missionNumber, shortName, actor);
            invokeExtensions("OnActorDestroyed", missionNumber, shortName, actor);
        }

        public override void OnActorDamaged(int missionNumber, string shortName, AiActor actor, AiDamageInitiator initiator, NamedDamageTypes damageType)
        {
            base.OnActorDamaged(missionNumber, shortName, actor, initiator, damageType);
            invokeExtensions("OnActorDamaged", missionNumber, shortName, actor, initiator, damageType);
        }

        public override void OnActorDead(int missionNumber, string shortName, AiActor actor, System.Collections.Generic.List<DamagerScore> damages)
        {
            base.OnActorDead(missionNumber, shortName, actor, damages);
            invokeExtensions("OnActorDead", missionNumber, shortName, actor, damages);
        }

        public override void OnActorTaskCompleted(int missionNumber, string shortName, AiActor actor)
        {
            base.OnActorTaskCompleted(missionNumber, shortName, actor);
            invokeExtensions("OnActorTaskCompleted", missionNumber, shortName, actor);
        }

        public override void OnTrigger(int missionNumber, string shortName, bool active)
        {
            base.OnTrigger(missionNumber, shortName, active);
            AiAction action;
            if (active && hasAction(missionNumber, shortName, out action))
                action.Do();

            invokeExtensions("OnTrigger", missionNumber, shortName, active);
        }

        private bool hasAction(int missionNumber, string shortName, out AiAction action)
        {
            action = GamePlay.gpGetAction(ActorName.Full(missionNumber, shortName));
            return action != null;
        }

        public override void OnAircraftDamaged(int missionNumber, string shortName, AiAircraft aircraft, AiDamageInitiator initiator, NamedDamageTypes damageType)
        {
            base.OnAircraftDamaged(missionNumber, shortName, aircraft, initiator, damageType);
            invokeExtensions("OnAircraftDamaged", missionNumber, shortName, aircraft, initiator, damageType);
        }

        public override void OnPlaceEnter(Player player, AiActor actor, int placeIndex)
        {
            base.OnPlaceEnter(player, actor, placeIndex);
            invokeExtensions("OnPlaceEnter", player, actor, placeIndex);
        }

        public override void OnPersonParachuteFailed(AiPerson person)
        {
            base.OnPersonParachuteFailed(person);
            invokeExtensions("OnPersonParachuteFailed", person);
        }

        public override void OnPersonParachuteLanded(AiPerson person)
        {
            base.OnPersonParachuteLanded(person);
            invokeExtensions("OnPersonParachuteLanded", person);
        }

        public override void OnPersonHealth(AiPerson person, AiDamageInitiator initiator, float deltaHealth)
        {
            base.OnPersonHealth(person, initiator, deltaHealth);
            invokeExtensions("OnPersonHealth", person, initiator, deltaHealth);
        }

        public override void OnPersonMoved(AiPerson person, AiActor fromCart, int fromPlaceIndex)
        {
            base.OnPersonMoved(person, fromCart, fromPlaceIndex);
            invokeExtensions("OnPersonMoved", person, fromCart, fromPlaceIndex);
        }

        public override void OnAircraftKilled(int missionNumber, string shortName, AiAircraft aircraft)
        {
            base.OnAircraftKilled(missionNumber, shortName, aircraft);
            invokeExtensions("OnAircraftKilled", missionNumber, shortName, aircraft);
        }

        public override void OnAircraftCrashLanded(int missionNumber, string shortName, AiAircraft aircraft)
        {
            base.OnAircraftCrashLanded(missionNumber, shortName, aircraft);
            invokeExtensions("OnAircraftCrashLanded", missionNumber, shortName, aircraft);
        }

        public override void OnAircraftLanded(int missionNumber, string shortName, AiAircraft aircraft)
        {
            base.OnAircraftLanded(missionNumber, shortName, aircraft);
            invokeExtensions("OnAircraftLanded", missionNumber, shortName, aircraft);
        }

        public override void OnAircraftTookOff(int missionNumber, string shortName, AiAircraft aircraft)
        {
            base.OnAircraftTookOff(missionNumber, shortName, aircraft);
            invokeExtensions("OnAircraftTookOff", missionNumber, shortName, aircraft);
        }

        public override void OnAircraftCutLimb(int missionNumber, string shortName, AiAircraft aircraft,
                                               AiDamageInitiator initiator, LimbNames limbName)
        {
            base.OnAircraftCutLimb(missionNumber, shortName, aircraft, initiator, limbName);
            invokeExtensions("OnAircraftCutLimb", missionNumber, shortName, aircraft, initiator, limbName);
        }

        public override void OnAircraftLimbDamaged(int missionNumber, string shortName, AiAircraft aircraft,
                                                   AiLimbDamage limbDamage)
        {
            base.OnAircraftLimbDamaged(missionNumber, shortName, aircraft, limbDamage);
            invokeExtensions("OnAircraftLimbDamaged", missionNumber, shortName, aircraft, limbDamage);
        }

        public override void OnOrderMissionMenuSelected(Player player, int id, int menuItemIndex)
        {
            base.OnOrderMissionMenuSelected(player, id, menuItemIndex);
            invokeExtensions("OnOrderMissionMenuSelected", player, id, menuItemIndex);
        }

        public override void OnSingleBattleSuccess(bool success)
        {
            base.OnSingleBattleSuccess(success);
            invokeExtensions("OnSingleBattleSuccess", success);
        }

        public override void OnAiAirNewEnemy(AiAirEnemyElement element, int army)
        {
            base.OnAiAirNewEnemy(element, army);
            invokeExtensions("OnAiAirNewEnemy", element, army);
        }

        public override void OnAutopilotOff(AiActor actor, int placeIndex)
        {
            base.OnAutopilotOff(actor, placeIndex);
            invokeExtensions("OnAutopilotOff", actor, placeIndex);
        }

        public override void OnAutopilotOn(AiActor actor, int placeIndex)
        {
            base.OnAutopilotOn(actor, placeIndex);
            invokeExtensions("OnAutopilotOn", actor, placeIndex);
        }

        public override void OnCarter(AiActor actor, int placeIndex)
        {
            base.OnCarter(actor, placeIndex);
            invokeExtensions("OnCarter", actor, placeIndex);
        }

        public override void OnPlaceLeave(Player player, AiActor actor, int placeIndex)
        {
            base.OnPlaceLeave(player, actor, placeIndex);
            invokeExtensions("OnPlaceLeave", player, actor, placeIndex);
        }

        public override void Timeout(double sec, DoTimeout doTimeout)
        {
            base.Timeout(sec, doTimeout);
            invokeExtensions("Timeout", sec, doTimeout);
        }

        public override void OnUserDeleteUserLabel(GPUserLabel ul)
        {
            base.OnUserDeleteUserLabel(ul);
            invokeExtensions("OnUserDeleteUserLabel", ul);
        }

        public override void OnUserCreateUserLabel(GPUserLabel ul)
        {
            base.OnUserCreateUserLabel(ul);
            invokeExtensions("OnUserCreateUserLabel", ul);
        }

        public override void OnBombExplosion(string title, double mass, Point3d pos, AiDamageInitiator initiator, int eventArgInt)
        {
            base.OnBombExplosion(title, mass, pos, initiator, eventArgInt);
            invokeExtensions("OnBombExplosion", title, mass, pos, initiator, eventArgInt);
        }

        public override void OnStationaryKilled(int missionNumber, GroundStationary stationary, AiDamageInitiator initiator, int eventArgInt)
        {
            base.OnStationaryKilled(missionNumber, stationary, initiator, eventArgInt);
            invokeExtensions("OnStationaryKilled", missionNumber, stationary, initiator, eventArgInt);
        }

        public override void OnBuildingKilled(string title, Point3d pos, AiDamageInitiator initiator, int eventArgInt)
        {
            base.OnBuildingKilled(title, pos, initiator, eventArgInt);
            invokeExtensions("OnBuildingKilled", title, pos, initiator, eventArgInt);
        }
        #endregion

        protected void Log(string text, params object[] args)
        {
            Log(this, text, args);
        }

        protected void Log(AMission mission, string text, params object[] args)
        {
            if (mission is XMission && !((XMission)mission).IsLogEnabled)
                return;

            if (args != null && args.Length > 0)
                mission.GamePlay.gpLogServer(null, text, safeArgs(args));
            else
                mission.GamePlay.gpLogServer(null, text, null);
        }

        private static object[] safeArgs(System.Collections.Generic.IList<object> args)
        {
            var result = new object[args.Count];
            for (var i = 0; i < result.Length; i++)
            {
                if (args[i] == null)
                    result[i] = "(null)";
                var s = args[i] as string;
                result[i] = s ?? args[i].ToString();
            }
            return result;
        }

        protected AiActor GetActor(string name)
        {
            var actor = GamePlay.gpActorByName(name);
            if (actor == null)
                Log("ERROR: Actor not found: '{0}'", name);
            return actor;

        }

        protected bool IsEveryNthSecond(int seconds)
        {
            return Time.tickCounter() % (seconds * TimeSecond) == 0;
        }

        protected void ConfigureIL2X(string typeName, string filePath)
        {
            // note: LINQ cannot be used in mission .cs files; anoying isn't it? :Op
            foreach (var x in _extensions)
            {
                if (x.Instance.GetType().FullName == typeName)
                {
                    invokeExtension(x, "ConfigureFromFile", filePath);
                }
            }
        }

        protected void SendTimedScreenMessage(string time, string interval, object recipients, string msg, params object[] args)
        {
            var xInfo = getFirstRunningExtension();
            if (xInfo == null)
            {
                Log("ERROR: Cannot invoke 'SendTimedHudMessage'. No IL2X is running at this time.");
                return;
            }
            var useArgs = prepareArgs(new[] { time, interval, msg, args, recipients });
            invokeExtension(xInfo, "SendTimedHudMessage", useArgs);
        }

        protected void SendTimedChatMessage(string time, string interval, object recipients, string msg, params object[] args)
        {
            var xInfo = getFirstRunningExtension();
            if (xInfo == null)
            {
                Log("ERROR: Cannot invoke 'SendTimedChatMessage'. No IL2X is running at this time.");
                return;
            }
            var useArgs = prepareArgs(new[] { time, interval, msg, args, recipients });
            invokeExtension(xInfo, "SendTimedChatMessage", useArgs);
        }

        /*
        Enums moved to enums.cs
        [Flags]
        public enum To { Hud = 1, Log = 2, HudAndLog = Hud | Log }
        public enum For { All = -1, Any = All, Unknown = 0, Red = 1, Blue = 2, England = Red, German = Blue }
        */
        public void Message(To to, string s, params object[] args)
        {
            Message(to, For.All, s, args);
        }

        public void Message(To to, For army, string s, params object[] args)
        {
            Message(to, GetArmyPlayers(army), s, args);
        }

        public void Message(To to, Player player, string s, params object[] args)
        {
            Player[] players = new Player[] { player };
            Message(to, players, s, args);
        }

        public void Message(To to, Player[] players, string s, params object[] args)
        {
            if ((to & To.Hud) == To.Hud)
            {
                GamePlay.gpHUDLogCenter(players, s, args);
            }
            if ((to & To.Log) != To.Log)
                return;

            GamePlay.gpLogServer(players, s, args);
        }

        private Player[] getArmyPlayers(int army)
        {
            if (IsSinglePlayerMode)
            {
                return (army <= (decimal)For.Unknown || army == GamePlay.gpPlayer().Army())
                    ? new[] { GamePlay.gpPlayer() }
                    : new Player[0];
            }
            var players = new System.Collections.Generic.List<Player>();
            foreach (var player in GamePlay.gpRemotePlayers())
            {
                if (army < 1 || army.Equals(player.Army()))
                    players.Add(player);
            }
            return players.ToArray();
        }

        protected void DisableTrigger(string shortName)
        {
            var trg = GamePlay.gpGetTrigger(shortName);
            if (trg != null)
                trg.Enable = false;
        }

        protected void EnableTrigger(string shortName)
        {
            var trg = GamePlay.gpGetTrigger(shortName);
            if (trg != null)
                trg.Enable = true;
        }

        protected Player[] GetArmyPlayers(For army, bool returnNullForAll = true)
        {
            if (army <= For.Unknown)
                return returnNullForAll ? null : getArmyPlayers(0);

            return getArmyPlayers((int)army);
        }

        protected XMission()
        {
            var myDocuments = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            docFolderPath = "1C SoftClub\\il-2 sturmovik cliffs of dover";
            IL2XRootFolder = System.IO.Path.Combine(myDocuments, docFolderPath + "\\IL2X");
        }
    }
}