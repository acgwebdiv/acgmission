﻿using System.Text;
using System;
using System.Collections;
using maddox.game;
using maddox.game.world;
using maddox.GP;
using part;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;

namespace ACGMission
{
    public class StandAloneMission : ScoringMission
    {
        public StandAloneMission()
        {
            _amCAMPAIGNMission = true;
            MissionLength = 180;
            _scoringSystem.InformAboutGroundKills = false;
        }

        public override void OnTickGame()
        {
            base.OnTickGame();
        }

        ///<summary>Can be overriden to write a information/welcome message to the players. It is called frequently based on the given ReoccuringMessageFrequence value.</summary>
        protected override void WriteReoccurringMessage()
        {
        }

        ///<summary>Can be overriden to write a mission status (e.g. current score) message to the players. It is called frequently based on the given MissionStatusMessageFrequence value.</summary>
        protected override void WriteMissionStatusMessage()
        {
        }

        ///<summary>override be overriden to write a message to the players on mission end. It is called automatically on mission end.</summary>
        protected override void WriteMissionEndMessage()
        {
        }

        ///<summary>Can be overriden to write a weclome or mssion intro message to the player. It is called once for each player when they spawn in the first time.</summary>
        protected override void WritePlayerWelcomeMessage(Player player)
        {
        }
    }
}