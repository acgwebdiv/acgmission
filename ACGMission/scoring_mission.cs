﻿using System.Text;
using System;
using System.Collections;
using maddox.game;
using maddox.game.world;
using maddox.GP;
using part;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;

namespace ACGMission
{
    public class ScoringMission : BaseMission
    {
        protected ScoringSystem _scoringSystem;

        public ScoringMission()
        {
            _scoringSystem = new ScoringSystem(this);
        }

        protected override void WriteMissionStatusMessage()
        {
            Message(To.Log, RemainingMissionTimeOutputString());
            Message(To.Log, CurrentScoreOutputString());
        }

        public string CurrentScoreOutputString()
        {
            return String.Format( "[Current Score]\n" +
                                  "          Royal Air Force: {0}\n" +
                                  "          Luftwaffe      : {1}", _scoringSystem.OverallScoreRed() , _scoringSystem.OverallScoreBlue());
        }

        protected override void ChatMessageWithCommandReceived(IPlayer player, string command, List<string> args)
        {
            base.ChatMessageWithCommandReceived(player, command, args);

            if (command == "score")
                Message(To.Log, player, CurrentScoreOutputString());
        }

        protected override void  WriteMissionEndMessage()
        {
            int scoreRed = _scoringSystem.OverallScoreRed();
            int scoreBlue = _scoringSystem.OverallScoreBlue();

            Message(To.HudAndLog, "[Final Score] RAF: " + scoreRed + " - LW: " + scoreBlue);

            String winnerMsg = "";

            if (scoreRed > scoreBlue)
                winnerMsg = "The Royal Air Force has won the mission!";
            else if (scoreRed < scoreBlue)
                winnerMsg = "The Luftwaffe has won the mission!";
            else
                winnerMsg = "The mission ends in a draw!";

            Timeout(10, () => Message(To.HudAndLog, winnerMsg));
            Timeout(20, () => Message(To.HudAndLog, winnerMsg));
            Timeout(30, () => Message(To.HudAndLog, winnerMsg));
            Timeout(40, () => Message(To.HudAndLog, winnerMsg));
            Timeout(60, () => Message(To.HudAndLog, Constants.AcgWebsite));
            Timeout(80, () => Message(To.HudAndLog, Constants.AcgWebsite));
        }

        public override void OnAircraftCrashLanded(int missionNumber, string shortName, AiAircraft aircraft)
        {
            base.OnAircraftCrashLanded(missionNumber, shortName, aircraft);
            _scoringSystem.AircraftLanded(aircraft, true);
        }

        public override void OnAircraftLanded(int missionNumber, string shortName, AiAircraft aircraft)
        {
            base.OnAircraftLanded(missionNumber, shortName, aircraft);
            _scoringSystem.AircraftLanded(aircraft, false);
        }

        public override void OnActorDead(int missionNumber, string shortName, AiActor actor, List<DamagerScore> damages)
        {
            base.OnActorDead(missionNumber, shortName, actor, damages);
            _scoringSystem.ActorDead(actor, "Dead");
        }

        public override void OnActorDestroyed(int missionNumber, string shortName, AiActor actor)
        {
            base.OnActorDestroyed(missionNumber, shortName, actor);
            _scoringSystem.ActorDead(actor, "Destroyed");
        }
        public override void OnBombExplosion(string title, double mass, Point3d pos, AiDamageInitiator initiator, int eventArgInt)
        {
            base.OnBombExplosion(title, mass, pos, initiator, eventArgInt);
            if (_scoringSystem.BombExplosion(mass, pos, initiator) == true)
            {
                _eventOutHandler.LogBombExplosion(GetGameTime().ToString("HH:mm:ss"), title, mass, pos, initiator, eventArgInt, true);
            } else
            {
                _eventOutHandler.LogBombExplosion(GetGameTime().ToString("HH:mm:ss"), title, mass, pos, initiator, eventArgInt, false);
            }
        }
    }
}