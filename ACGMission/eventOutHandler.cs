﻿using maddox.game;
using maddox.game.world;
using maddox.GP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ACGMission
{
    public class eventOutHandler
    {
        private StringBuilder DNKOUTSTRING = new StringBuilder("");
        private string eventOutPath;




        public eventOutHandler(string outpath, bool isCampaign = false)
        {
            
            
            if (isCampaign == true)
            {
                outpath = outpath + "\\Campaign";
            } else
            {
                outpath = outpath + "\\Public";
            }

            //Create if not available
            Directory.CreateDirectory(outpath);

            eventOutPath = outpath + "\\EventOut.txt";
            if (File.Exists(eventOutPath) == true)
            {
                File.Delete(eventOutPath);
            }
            //File.AppendAllText(eventOutPath, "EventOut type:" + typeNum.ToString());
        }

        public void writeEventOut()
        {
            try
            {
                File.AppendAllText(eventOutPath, DNKOUTSTRING.ToString());
                DNKOUTSTRING.Clear();
            }
            catch
            {
                Console.WriteLine("Issue writing eventout.");
            }
        }

        public void DNKOUT(string message)
        {
            try
            {
                DNKOUTSTRING.Append(message + System.Environment.NewLine);
            }
            catch
            {

            }
        }

        string GetDamager(DamagerScore damager)
        {
            string damagerName;
            string faction;
            AiActor AiDamager = damager.initiator.Actor;
            if (AiDamager is AiAircraft)
            {
                AiAircraft ac = AiDamager as AiAircraft;
                if (damager.initiator.Player != null)
                {
                    damagerName = "Human," + damager.initiator.Player.Name() + "," + ac.InternalTypeName().Replace("bob:Aircraft.", "");
                }
                else
                {
                    damagerName = "AI," + ac.Person(0).Name() + "," + ac.InternalTypeName().Replace("bob:Aircraft.", "");
                }
                faction = "Unknown";
                if (ac.Army() == (int)Army.Red)
                {
                    faction = "RAF";
                }
                if (ac.Army() == (int)Army.Blue)
                {
                    faction = "LW";
                }
                damagerName = damagerName + "," + faction;
            }
            else if (AiDamager is AiGroundActor)
            {
                AiGroundActor ga = AiDamager as AiGroundActor;
                if (damager.initiator.Player != null)
                {
                    damagerName = "Human," + damager.initiator.Player.Name() + "," + ga.Type().ToString();
                }
                else
                {
                    damagerName = "AI," + ga.Name() + "," + ga.Type().ToString();
                }
                faction = "Unknown";
                if (ga.Army() == (int)Army.Red)
                {
                    faction = "RAF";
                }
                if (ga.Army() == (int)Army.Blue)
                {
                    faction = "LW";
                }
                damagerName = damagerName + "," + faction;
            }
            else
            {
                damagerName = "Unknown,Unknown,Unknown";
            }
            return damagerName;
        }

        private bool isAiControlledPlane(AiAircraft aircraft)
        {
            if (aircraft == null)
            {
                return false;
            }

            for (int i = 0; i < aircraft.Places(); i++)
            {
                if (aircraft.Player(i) != null)
                {
                    return false;
                }
            }

            return true;
        }

        public string GetAircraftDetails(AiAircraft ac)
        {
            string retDetails = "Exception";
            try
            {
                string name = "";
                string faction = "LW";
                string aircraft = "Unknown Aircraft";
                string assetName = "";
                if (isAiControlledPlane(ac) == false)
                {
                    name = "Human," + ac.Person(0).Player().Name();
                }
                else
                {
                    name = "AI,AIPilot";
                }
                assetName = ac.Name();
                aircraft = ac.InternalTypeName().Replace("bob:Aircraft.", "");
                if (ac.Army() == (int)Army.Red)
                {
                    faction = "RAF";
                }
                retDetails = name + "," + aircraft + "," + assetName + "," + faction;
            }
            catch
            {
                DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "| Exception thrown in GetAircraftDetail, notJustHuman.");
            }
            return retDetails;
        }
        
        public string GetGroundDetails( AiGroundActor ground)
        {
            string retDetails = "Exception";
            try
            {
                string faction = "LW";
                if (ground.Army() == (int)Army.Red)
                {
                    faction = "RAF";
                }
                retDetails = ground.Name() + "," + ground.Type().ToString() + "," + faction;
            }
            catch
            {
                DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "| Exception thrown in GetGroundDetail, notJustHuman.");
            }
            return retDetails;
        }

        public string GetInitiatorAircraftDetails(AiDamageInitiator initiator)
        {
            string retDetails = "Exception";
            try
            {
                if (initiator.Actor is AiAircraft) //Only capture Aircraft initiators?.
                {
                    retDetails = GetAircraftDetails(initiator.Actor as AiAircraft);
                }
                if (initiator.Actor is AiGroundActor) 
                {
                    retDetails = GetGroundDetails(initiator.Actor as AiGroundActor);
                }
            }
            catch
            {
                DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "| Exception thrown in GetInitiatorDetails.");
            }
            return retDetails;
        }




        public void LogPlayerConnected(string missionTime, Player player)
        {
            try
            {
                string capture = DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|CONNECTED|" + player.Name();
                DNKOUT(capture);
            }
            catch
            {
                DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|Exception caught in OnPlayerConnected");
            }
        }

        public void LogPlayerDisconnected(string missionTime, Player player, string diagnostic)
        {
            try
            {
                string capture = DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|DISCONNECTED|" + player.Name() + "|" + diagnostic;
                DNKOUT(capture);
            }
            catch
            {
                DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|Exception caught in OnPlayerDisconnected");
            }
        }

        public void LogActorCreatedDetail(string missionTime, AiActor actor)
        {
            if (actor is AiAircraft)
            {
                AiAircraft ac = (AiAircraft)actor;
                string x = Math.Round(ac.Pos().x, 0).ToString();
                string y = Math.Round(ac.Pos().y, 0).ToString();
                string details = GetAircraftDetails(ac);
                if (details != "Exception")
                {
                    DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|ActorCreated|" + x + "|" + y + "|" + details);
                }
                else
                {
                    DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|Exception thrown in OnActorCreated");
                }
            }
        }

        public void LogAircraftTookOff(string missionTime, AiAircraft aircraft)
        {
            string x = Math.Round(aircraft.Pos().x, 0).ToString();
            string y = Math.Round(aircraft.Pos().y, 0).ToString();
            string details = GetAircraftDetails(aircraft);
            if (details != "Exception")
            {
                DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|TAKEOFF|" + details + "|" + x + "|" + y);
            }
            else
            {
                DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|Exception caught in OnAircraftTookOff");
            }
        }

        public void LogAircraftLanded(string missionTime, AiAircraft aircraft, string outcome, bool crash = false)
        {
            string landingType = "LANDED";
            if (crash == true)
            {
                landingType = "CRASHLANDED";
            }
            string details = GetAircraftDetails(aircraft);
            if (details != "Exception")
            {
                string x = Math.Round(aircraft.Pos().x, 0).ToString();
                string y = Math.Round(aircraft.Pos().y, 0).ToString();
                DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|" + landingType + "|" + details + "|" + x + "|" + y + "|" + outcome);
            }
            else
            {
                DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|Exception caught in OnAircraftLanded");
            }
        }

        public void LogAircraftLandedScore(AMission mission, string missionTime, AiAircraft aircraft, bool crash = false)
        {
            string landingType = "LANDED";
            if (crash == true)
            {
                landingType = "CRASHLANDED";
            }
            string details = GetAircraftDetails(aircraft);
            if (details != "Exception")
            {
                string x = Math.Round(aircraft.Pos().x, 0).ToString();
                string y = Math.Round(aircraft.Pos().y, 0).ToString();

                if (mission.Battle.GetDamageInitiators(aircraft) != null)
                {
                    string shared = (mission.Battle.GetDamageInitiators(aircraft).Count > 1 ? "Shared" : "Not shared");
                    foreach (DamagerScore d in mission.Battle.GetDamageInitiators(aircraft))
                    {
                        DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|" + landingType + " - SCORE|" + GetAircraftDetails(aircraft) + "|" + x + "|" + y + "|" + GetDamager(d) + "(" + shared + ")");
                    }
                }
            }
            else
            {
                DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|Exception caught in OnAircraftLandedScore");
            }
        }

        public void LogPersonHealth(string missionTime, AiPerson person, float deltaHealth)
        {
            string health;
            if (person != null)
            {
                health = person.Health.ToString() + " health";
                try
                {
                    if (person.Player() != null)
                    {
                        if (person.Health == 0)
                        {
                            DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|DEAD|" + person.Player().Name() + "|" + health);
                        }
                        else
                        {
                            DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|INJURED|" + person.Player().Name() + "|" + health);
                        }
                    }
                }
                catch
                {
                    DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|Exception caught in OnPersonHealth");
                }
            }
        }

        public void LogAircraftDamaged(string missionTime, AiAircraft aircraft, AiDamageInitiator initiator, part.NamedDamageTypes damageType)
        {
            try
            {
                string initiatorDetails = GetInitiatorAircraftDetails(initiator);
                string details = GetAircraftDetails(aircraft);
                DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|AircraftDamaged|" + details + "|" + damageType.ToString() + "|" + initiatorDetails);
            }
            catch
            {
                DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|Exception caught in AircraftDamaged");
            }
        }

        public void LogAircraftCutLimb(string missionTime, AiAircraft aircraft, AiDamageInitiator initiator, part.LimbNames limbName)
        {
            try
            {
                string initiatorDetails = GetInitiatorAircraftDetails(initiator);
                string details = GetAircraftDetails(aircraft);
                DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|AircraftCutLimb|" + details + "|" + limbName.ToString() + "|" + initiatorDetails);
            }
            catch
            {
                DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|Exception caught in AircraftCutLimb");
            }
        }

        public void LogAircraftLimbDamaged(string missionTime, AiAircraft aircraft, AiLimbDamage limbDamage)
        {
            try
            {
                string details = GetAircraftDetails(aircraft);
                DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|AircraftLimbDamaged|" + details + "|" + ((part.LimbNames)limbDamage.LimbId).ToString());
            }
            catch
            {
                DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|Exception caught in AircraftLimbDamaged");
            }
        }

        public void IsThisAJumper(string missionTime, AiAircraft aircraft, Player player)
        {
            if (isAiControlledPlane(aircraft) == true)  //The plane is now under AI control
            {
                string x = Math.Round(aircraft.Pos().x, 0).ToString();
                string y = Math.Round(aircraft.Pos().y, 0).ToString();
                try
                {
                    if (aircraft != null)
                    {
                        string capture;
                        if (aircraft.Person(0) == null)
                        {
                            capture = DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|JUMPED|" + player.Name() + "|" + x + "|" + y;
                            DNKOUT(capture);
                        }
                    }
                }
                catch
                {
                    DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|Exception caught in didHeJump");
                }
            }
        }

        public void LogPersonParachuteFailed(string missionTime, AiPerson person, string outcome = null)
        {
            try
            {
                string x = Math.Round(person.Pos().x, 0).ToString();
                string y = Math.Round(person.Pos().y, 0).ToString();

                Player player = person.Player();
                string name = "AIPilot";
                if (player != null)
                {
                    name = player.Name();
                }
                string faction;
                try
                {
                    faction = "LW";
                    if (person.Army() == (int)Army.Red)
                    {
                        faction = "RAF";
                    }
                }
                catch
                {
                    faction = "UNKNOWN";
                }
                string capture = DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|PARACHUTEFAILED|" + name + "(" + faction + ")" + "|" + x + "|" + y + "|" + outcome;
                DNKOUT(capture);
            }
            catch
            {
                DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|Exception caught in OnPersonParachuteFailed");
            }
        }

        public void LogPersonParachuteLanded(string missionTime, AiPerson person, string outcome = null)
        {
            try
            {
                string x = Math.Round(person.Pos().x, 0).ToString();
                string y = Math.Round(person.Pos().y, 0).ToString();

                Player player = person.Player();
                string name = "AIPilot";
                if (player != null)
                {
                    name = player.Name();
                }
                string faction;
                try
                {
                    faction = "LW";
                    if (person.Army() == (int)Army.Red)
                    {
                        faction = "RAF";
                    }
                }
                catch
                {
                    faction = "UNKNOWN";
                }
                string capture = DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|PARACHUTELANDED|" + name + "(" + faction + ")" + "|" + x + "|" + y + "|" + outcome;
                DNKOUT(capture);
            }
            catch
            {
                DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|Exception caught in OnPersonParachuteLanded");
            }
        }

        public void LogBombExplosion(string missionTime, string title, double mass, Point3d pos, AiDamageInitiator initiator, int eventArgInt, bool scores = false)
        {
            string x = Math.Round(pos.x, 0).ToString();
            string y = Math.Round(pos.y, 0).ToString();
            string details = GetInitiatorAircraftDetails(initiator);

            if (details != "Exception")
            {
                DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|BombExplosion - " + title + "|" + x + "|" + y + "|" + details + "|" + scores.ToString());
            }
            else
            {
                DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|Exception thrown in OnBombExplosion");
            }
        }

        public void LogActorDeadDestroyed(AMission mission, string missionTime, AiActor actor, string DeadDestroyed)
        {
            string x = Math.Round(actor.Pos().x, 0).ToString();
            string y = Math.Round(actor.Pos().y, 0).ToString();

            if (actor is AiAircraft)
            {
                AiAircraft aircraft = actor as AiAircraft;
                string details = GetAircraftDetails(aircraft);
                if (details != "Exception")
                {
                    if (mission.Battle.GetDamageInitiators(actor) != null)
                    {
                        string shared = (mission.Battle.GetDamageInitiators(aircraft).Count > 1 ? "Shared" : "Not shared");
                        foreach (DamagerScore d in mission.Battle.GetDamageInitiators(aircraft))
                        {
                            DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|Actor" + DeadDestroyed + " - " + details + "|" + x + "|" + y + "|" + GetDamager(d) + "(" + shared + ")");
                        }
                    }
                }
                else
                {
                    DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|Exception thrown in LogActorDeadDestroyed");
                }
            }
            else
            {
                AiGroundActor ground = actor as AiGroundActor;
                string details = GetGroundDetails(ground);
                if (details != "Exception")
                {
                    if (mission.Battle.GetDamageInitiators(actor) != null)
                    {
                        string shared = (mission.Battle.GetDamageInitiators(ground).Count > 1 ? "Shared" : "Not shared");
                        foreach (DamagerScore d in mission.Battle.GetDamageInitiators(ground))
                        {
                            DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|GroundActor" + DeadDestroyed + " - " + details + "|" + x + "|" + y + "|" + GetDamager(d) + "(" + shared + ")");
                        }
                    }
                }
                else
                {
                    DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|Exception thrown in OnActorDead");
                }
            }
        }

        public void LogAircraftKilled(AMission mission, string missionTime, AiAircraft aircraft)
        {
            string x = Math.Round(aircraft.Pos().x, 0).ToString();
            string y = Math.Round(aircraft.Pos().y, 0).ToString();
            string details = GetAircraftDetails(aircraft);
            if (details != "Exception")
            {
                if (mission.Battle.GetDamageInitiators(aircraft) != null)
                {
                    string shared = (mission.Battle.GetDamageInitiators(aircraft).Count > 1 ? "Shared" : "Not shared");
                    foreach (DamagerScore d in mission.Battle.GetDamageInitiators(aircraft))
                    {
                        DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|AircraftKilled - " + details + "|" + x + "|" + y + "|" + GetDamager(d) + "(" + shared + ")");
                    }
                }
            }
            else
            {
                DNKOUT(DateTime.Now.ToString("HH:mm:ss") + "|" + missionTime + "|Exception thrown in OnAircraftKilled");
            }
        }

    }
}