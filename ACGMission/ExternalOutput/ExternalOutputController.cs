﻿using maddox.game;
using maddox.game.world;
using System;
using System.Collections.Generic;

namespace ACGMission.ExternalOutput
{
    internal sealed class ExternalOutputController
    {
        private object mylock = new object();

        private ExternalOutputServer _server;

        private BaseMission _mission;
        public BaseMission Mission
        {
            get
            {
                BaseMission result;
                lock (mylock)
                {
                    result = _mission;
                }
                return result;
            }
            private set
            {
                lock (mylock)
                {
                    _mission = value;
                }
            }
        }
        private List<ExternalOutputPilot> _AllPilots = new List<ExternalOutputPilot>();
        public List<ExternalOutputPilot> AllPilots
        {
            get
            {
                List<ExternalOutputPilot> result;
                lock (mylock)
                {
                    result = _AllPilots;
                }
                return result;
            }
            private set
            {
                lock (mylock)
                {
                    _AllPilots = value;
                }
            }
        }

        private List<ExternalOutputGroup> _AIGroups = new List<ExternalOutputGroup>();
        public List<ExternalOutputGroup> AIGroups
        {
            get
            {
                List<ExternalOutputGroup> result;
                lock (mylock)
                {
                    result = _AIGroups;
                }
                return result;
            }
            private set
            {
                lock (mylock)
                {
                    _AIGroups = value;
                }
            }
        }

        //DONKEY - Adding extrainfo to ExternalOutput
        private int _activePlayers;
        public int ActivePlayers
        {
            get
            {
                int result;
                lock (mylock)
                {
                    result = _activePlayers;
                }
                return result;
            }
            private set
            {
                lock (mylock)
                {
                    _activePlayers = value;
                }
            }
        }
        public ExternalOutputController(BaseMission mission)
        {
            killList = new List<string>();
            Mission = mission;
            _server = new ExternalOutputServer(this, "DATA");
            _server = new ExternalOutputServer(this, "BA");
            _server = new ExternalOutputServer(this, "DS");
    }

        private List<string> _killList;
        public List<string> killList
        {
            get
            {
                List<string> result;
                lock (mylock)
                {
                    result = _killList;
                }
                return result;
            }
            private set
            {
                lock (mylock)
                {
                    _killList = value;
                }
            }
        }

        private string _missionStartTime;
        public string missionStartTime
        {
            get
            {
                string result;
                lock (mylock)
                {
                    result = _missionStartTime;
                }
                return result;
            }
            private set
            {
                lock (mylock)
                {
                    _missionStartTime = value;
                }
            }
        }

        public void setKillList(List<string> kl)
        {
            lock (mylock)
            {
                try
                {
                    killList = kl;
                }
                catch (Exception e)
                {
                    _mission.GamePlay.gpLogServer(null, "Error setting kill list : " + e.Message + " | " + e.StackTrace, null);
                }

            }
        }


        public void setCurrentMissionData(string missionStart, List<UniquePilot> pilots, int playersInGame, Dictionary<string, AiWayPoint[]> AIGroupData, Dictionary<string, int> AICurrWP)
        {
            lock (mylock)
            {
                try
                {
                    missionStartTime = missionStart;
                    ActivePlayers = playersInGame;
                    AllPilots.Clear();
                    foreach (var rr in pilots)
                    {
                        AllPilots.Add(new ExternalOutputPilot(rr));
                    }
                    AIGroups.Clear();

                    foreach (string aigrp in AIGroupData.Keys)
                    {
                        AiWayPoint[] wps = AIGroupData[aigrp];
                        int curr = AICurrWP[aigrp];
                        AIGroups.Add(new ExternalOutputGroup(aigrp, wps, curr));
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error setting mission data : " + e.Message);
                }
            }
        }
    }
}
