﻿using maddox.game.world;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ACGMission.ExternalOutput
{

    [DataContract]
    class ExternalOutputResponse
    {
        [DataMember(Name = "MissionTime", IsRequired = true)]
        public string MissionTime { get; set; }

        [DataMember(Name = "MissionStart", IsRequired = true)]
        public string MissionStart { get; set; }

        [DataMember(Name = "MissionDate", IsRequired = true)]
        public string MissionDate { get; set; }

        [DataMember(Name = "ActivePlayers", IsRequired = true)]
        public string ActivePlayers { get; set; }

        [DataMember(Name = "UniqueAircraft", IsRequired = false)]
        public List<ExternalOutputPilot> UniqueAircraft { get; set; }

        [DataMember(Name = "GroupWaypoints", IsRequired = false)]
        public List<ExternalOutputGroup> GroupWaypoints { get; set; }
    }

    [DataContract]
    class ExternalOutputPilot
    {
        [DataMember(Name = "I", IsRequired = true)]
        public string I { get; set; }
        [DataMember(Name = "A", IsRequired = true)]
        public string A { get; set; }
        [DataMember(Name = "G", IsRequired = false)]
        public string G { get; set; }
        [DataMember(Name = "F", IsRequired = true)]
        public string F { get; set; }
        [DataMember(Name = "T", IsRequired = true)]
        public string T { get; set; }
        [DataMember(Name = "M", IsRequired = true)]
        public string M { get; set; }

        [DataMember(Name = "X", IsRequired = true)]
        public string X { get; set; }
        [DataMember(Name = "Y", IsRequired = true)]
        public string Y { get; set; }
        [DataMember(Name = "Z", IsRequired = true)]
        public string Z { get; set; }

        public ExternalOutputPilot(UniquePilot r)
        {
            I = r.Id;
            A = r.IsAI;
            G = r.GrpName;
            F = r.Faction;
            T = r.Type;
            M = r.MapGrid;
            X = r.X;
            Y = r.Y;
            Z = r.Z;
        }
    }

    [DataContract]
    class ExternalOutputGroup
    {
        [DataMember(Name = "I", IsRequired = false)]
        public string I { get; set; }
        [DataMember(Name = "W", IsRequired = false)]
        public string[] W { get; set; }
        [DataMember(Name = "C", IsRequired = false)]
        public string C { get; set; }


        public ExternalOutputGroup(string GroupName, AiWayPoint[] wps, int curr)
        {
            I = GroupName;
            C = curr.ToString();
            List<string> locwps = new List<string>();
            foreach (AiWayPoint w in wps)
            {
                string wpStr = ((int)(w.P.x)).ToString() + "|" + ((int)(w.P.y)).ToString() + "|" + ((int)(w.P.z)).ToString() + "|" + ((int)(w.Speed)).ToString();
                locwps.Add(wpStr);
            }
            W = locwps.ToArray();
        }
    }

    [DataContract]
    public class BA_BomberSpawn
    {
        [DataMember(Name = "ID", IsRequired = false)]
        public string ID;
        [DataMember(Name = "Detail", IsRequired = false)]
        public string[] Detail;
        [DataMember(Name = "Waypoints", IsRequired = false)]
        public string[] Waypoints;
        [DataMember(Name = "Statics", IsRequired = false)]
        public BA_StaticDef[] Statics;
        [DataMember(Name = "Chiefs", IsRequired = false)]
        public BA_ChiefDef[] Chiefs;
        [DataMember(Name = "Roads", IsRequired = false)]
        public BA_RoadDef[] Roads;
    }

    [DataContract]
    public class BA_StaticDef
    {
        [DataMember(Name = "Static", IsRequired = false)]
        public string Static;
        [DataMember(Name = "Detail", IsRequired = false)]
        public string Detail;
    }

    [DataContract]
    public class BA_ChiefDef
    {
        [DataMember(Name = "Chief", IsRequired = false)]
        public string Chief;
        [DataMember(Name = "Detail", IsRequired = false)]
        public string Detail;
    }

    [DataContract]
    public class BA_RoadDef
    {
        [DataMember(Name = "Chief", IsRequired = false)]
        public string Chief;
        [DataMember(Name = "Road", IsRequired = false)]
        public string Road;
    }

    [DataContract]
    class BA_KillList
    {
        [DataMember(Name = "ListOFIDs", IsRequired = true)]
        public string[] ListOFIDs;
    }
}
