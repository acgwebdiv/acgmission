﻿using maddox.game;
using maddox.game.world;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;


namespace ACGMission.ExternalOutput
{
    class ExternalOutputServer
    {
        private ExternalOutputController _controller;
        private Thread _thread;
        private string _serverType;

        public ExternalOutputServer(ExternalOutputController controller, string serverType)
        {
            _serverType = serverType;
            _controller = controller;
            _thread = new Thread(new ThreadStart(DoWork));
            _thread.Start();
        }

        private void DoWork()
        {
            if (_serverType == "DATA")
            {
                TcpListener listener = new TcpListener(IPAddress.Any, _controller.Mission.DATAPORT);
                listener.Start();
                while (true)
                {//DONKEY - added a try-catch to prevent port-blocking when cycling server quickly
                    try
                    {
                        using (TcpClient client = listener.AcceptTcpClient())
                        {
                            try  //Double-try to isolate GC blackout issue with read-lock.
                            {
                                //Request to send info back to an RAF GC
                                DataContractJsonSerializer js = new DataContractJsonSerializer(typeof(ExternalOutputResponse));
                                js.WriteObject(client.GetStream(), CreateResponse());
                                client.Close();
                            }
                            catch
                            {
                                client.Close();
                            }
                        }
                    }
                    catch
                    {
                    }
                }
            }

            if (_serverType == "BA")
            {
                TcpListener listener = new TcpListener(IPAddress.Any, _controller.Mission.BOMBERSAWAYPORT);
                listener.Start();
                while (true)
                {//DONKEY - added a try-catch to prevent port-blocking when cycling server quickly
                    try
                    {
                        using (TcpClient client = listener.AcceptTcpClient())
                        {
                            client.ReceiveTimeout = 2000;
                            NetworkStream serverStream = client.GetStream();
                            try  //Double-try to isolate GC blackout issue with read-lock.
                            {
                                //WE ARE FEEDING BOMBERS
                                string AsciiStream = new StreamReader(serverStream, Encoding.ASCII).ReadToEnd();
                                MemoryStream finalStream = new MemoryStream(Encoding.ASCII.GetBytes(AsciiStream));
                                DataContractJsonSerializer js = new DataContractJsonSerializer(typeof(BA_BomberSpawn));
                                BA_BomberSpawn bs = (BA_BomberSpawn)js.ReadObject(finalStream);
                                //Launch them here
                                spawnBombers(bs);
                            }
                            catch
                            {
                                client.Close();
                            }
                        }
                    }
                    catch
                    {
                    }
                }
            }

            if (_serverType == "DS")
            {
                TcpListener listener = new TcpListener(IPAddress.Any, _controller.Mission.DESPAWNPORT);
                listener.Start();
                while (true)
                {//DONKEY - added a try-catch to prevent port-blocking when cycling server quickly
                    try
                    {
                        using (TcpClient client = listener.AcceptTcpClient())
                        {
                            client.ReceiveTimeout = 2000;
                            NetworkStream serverStream = client.GetStream();
                            try  //Double-try to isolate GC blackout issue with read-lock.
                            {
                                //WE ARE KILLING BOMBERS
                                string AsciiStream = new StreamReader(serverStream, Encoding.ASCII).ReadToEnd();
                                //Console.WriteLine("Got kill/message list of " + AsciiStream);
                                MemoryStream finalStream = new MemoryStream(Encoding.ASCII.GetBytes(AsciiStream));
                                DataContractJsonSerializer js = new DataContractJsonSerializer(typeof(BA_KillList));
                                BA_KillList kl = (BA_KillList)js.ReadObject(finalStream);
                                //Launch them here
                                despawnBombersPrep(kl);
                            }
                            catch
                            {
                                client.Close();
                            }
                        }
                    }
                    catch
                    {
                    }
                }
            }
        }

        private ExternalOutputResponse CreateResponse()
        {
            try
            {
                ExternalOutputResponse resp = new ExternalOutputResponse();
                resp.MissionStart = _controller.missionStartTime;

                resp.MissionTime = _controller.Mission.GetGameTime().ToString("HH:mm:ss");
                resp.MissionDate = "01-Sep-1940";
                resp.UniqueAircraft = _controller.AllPilots;
                resp.ActivePlayers = _controller.ActivePlayers.ToString();
                resp.GroupWaypoints = _controller.AIGroups;
                return resp;
            }
            catch
            {
                return null;
            }
        }
        private void spawnBombers(BA_BomberSpawn bs)
        {
            try
            {
                ISectionFile f = _controller.Mission.GamePlay.gpCreateSectionFile();

                //statics.  Make sure we don't spawn within 150m of another static.
                foreach (var stats in bs.Statics)
                {
                    bool clearOfStatics = false;
                    double tgt_x;
                    double tgt_y;
                    if (double.TryParse(stats.Detail.Split(' ')[2], out tgt_x) && double.TryParse(stats.Detail.Split(' ')[3], out tgt_y))
                    {
                        GroundStationary[] localToTarget = _controller.Mission.GamePlay.gpGroundStationarys(tgt_x, tgt_y, 150);
                        if (localToTarget.Count() == 0)
                        {
                            clearOfStatics = true;
                        }
                    }
                    if (clearOfStatics)
                    {
                        f.add("Stationary", stats.Static, stats.Detail);
                        Console.WriteLine("Adding " + stats.Static + " : " + stats.Detail + " to spawn list");
                    }
                }
                _controller.Mission.GamePlay.gpPostMissionLoad(f);

                // airgroups section
                if (bs.ID != "")
                {
                    f.add("AirGroups", bs.ID, null);
                    // each airgroups info
                    foreach (var str in bs.Detail)
                    {
                        f.add(bs.ID, str.Split(':')[0].Trim(), str.Split(':')[1].Trim());
                    }
                    // airgroup waypoints
                    string wptype;
                    foreach (var str in bs.Waypoints)
                    {
                        wptype = str.Split(' ')[0];
                        f.add(bs.ID + "_Way", wptype, str.Replace(wptype + " ", "").Trim());

                        if (wptype == "GATTACK_TARG")
                        {
                            double tgt_x;
                            double tgt_y;
                            if (double.TryParse(str.Split(' ')[1], out tgt_x) && double.TryParse(str.Split(' ')[2], out tgt_y))
                            {
                                spawnStaticAt(tgt_x, tgt_y);
                            }
                        }
                    }
                }
                f = _controller.Mission.GamePlay.gpCreateSectionFile();
                _controller.Mission.GamePlay.gpPostMissionLoad(f);

                // load the mission stub
            }
            catch (Exception e)
            {
                _controller.Mission.GamePlay.gpLogServer(null, e.Message, null);
            }
        }

        private void spawnStaticAt(double x, double y)
        {
            ISectionFile f = _controller.Mission.GamePlay.gpCreateSectionFile();

            bool clearOfStatics = false;

            GroundStationary[] localToTarget = _controller.Mission.GamePlay.gpGroundStationarys(x, y, 200);
            if (localToTarget.Count() == 0)
            {
                clearOfStatics = true;
            }

            if (clearOfStatics)
            {
                LandTypes lt = _controller.Mission.GamePlay.gpLandType(x, y);
                string detail = "Stationary.Bedford_OY_open gb " + x.ToString() + " " + y.ToString();// +" 0.00"; //try zero?  Was 90.  Trying nothing
                if (lt == LandTypes.WATER)
                {
                    detail = "ShipUnit.Tanker_Medium1 gb " + x.ToString() + " " + y.ToString();// + " 0.00"; //try zero?  Was 60.  Trying nothing
                }

                f.add("Stationary", "DTarget", detail);
                Console.WriteLine("Spawned static target at " + x.ToString() + ", " + y.ToString());
                _controller.Mission.GamePlay.gpPostMissionLoad(f);
            }

        }


        private void despawnBombersPrep(BA_KillList kl)
        {
            try
            {
                List<string> killList = new List<string>();
                foreach (string ID in kl.ListOFIDs)
                {
                    //MSG:TO-XXX:ALL:Msg
                    //MSG:TO-XXX:ARMY:RAF:Msg
                    //MSG:TO-XXX:ARMY:LW:Msg
                    //MSG:TO-XXX:PlayerName:Msg
                    if (ID.ToUpper().Contains("MSG:") == true)
                    {
                        To whereTo;
                        whereTo = To.Log;
                        string whereToString;
                        whereToString = ID.Split(':')[1];
                        if (whereToString.ToUpper().Contains("H") == true)
                        {
                            whereTo = To.Hud;
                            if (whereToString.ToUpper().Contains("L") == true)
                            {
                                whereTo = To.HudAndLog;
                            }
                        }

                        if ((ID.ToUpper().Contains("ALL:") == true) || (ID.ToUpper().Contains("ARMY:") == true))
                        {
                            if (ID.ToUpper().Contains("ARMY:") == true)
                            {
                                string side = ID.Split(':')[3];
                                For army = For.Blue;
                                if ((side == "RAF") || (side.ToUpper() == "ENGLAND") || (side.ToUpper() == "RED"))
                                {
                                    army = For.Red;
                                }
                                string msg = ID.Split(':')[4];
                                //Message(whereTo, army, msg);

                            }
                            else
                            {
                                string msg = ID.Split(':')[3];
                                //Message(whereTo, msg);
                            }
                        }
                        else
                        {
                            string player = ID.Split(':')[2];
                            string msg = ID.Split(':')[3];
                            //Message(whereTo, player, msg);
                        }
                    }
                    else
                    {
                        killList.Add(ID);
                        Console.WriteLine("Asked to kill " + ID);
                    }
                }
                _controller.setKillList(killList);
                Console.WriteLine("Set Despawn KillList");
            }
            catch (Exception e)
            {
                _controller.Mission.GamePlay.gpLogServer(null, e.Message, null);
                Console.WriteLine("KILL LIST ISSUE");
                Console.WriteLine(e.Message);
            }
        }

        private Player[] getArmyPlayers(int army)
        {
            var players = new System.Collections.Generic.List<Player>();
            foreach (var player in _controller.Mission.GamePlay.gpRemotePlayers())
            {
                if (army < 1 || army.Equals(player.Army()))
                    players.Add(player);
            }
            return players.ToArray();
        }

        private Player[] GetArmyPlayers(For army, bool returnNullForAll = true)
        {
            if (army <= For.Unknown)
                return returnNullForAll ? null : getArmyPlayers(0);

            return getArmyPlayers((int)army);
        }

        private enum To
        {
            Hud = 1,
            Log = 2,
            HudAndLog = Hud | Log
        }

        private enum For
        {
            All = -1,
            Any = All,
            Unknown = 0,
            Red = 1,
            Blue = 2,
            England = Red,
            German = Blue,
            RAF = Red,
            LW = Blue
        }

        private void Message(To to, string s, params object[] args)
        {
            Message(to, For.All, s, args);
        }

        private void Message(To to, For army, string s, params object[] args)
        {
            Message(to, GetArmyPlayers(army), s, args);
        }

        private void Message(To to, Player player, string s, params object[] args)
        {
            Player[] players = new Player[] { player };
            Message(to, players, s, args);
        }

        private void Message(To to, Player[] players, string s, params object[] args)
        {
            if ((to & To.Hud) == To.Hud)
            {
                _controller.Mission.GamePlay.gpHUDLogCenter(players, s, args);
            }
            if ((to & To.Log) != To.Log)
                return;

            _controller.Mission.GamePlay.gpLogServer(players, s, args);
        }

    }
}
