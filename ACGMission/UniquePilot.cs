﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACGMission
{
    internal sealed class UniquePilot
    {
        public string Id { get; set; }
        public string IsAI { get; set; }
        public string GrpName { get; set; }
        public string Faction { get; set; }
        public string Type { get; set; }
        public string MapGrid { get; set; }
        public string X { get; set; }
        public string Y { get; set; }
        public string Z { get; set; }

        public string P { get; set; }
        public string R { get; set; }
        public string Yw { get; set; }
    }
}
