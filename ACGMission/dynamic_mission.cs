﻿using maddox.game;
using maddox.game.world;
using maddox.GP;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;


namespace ACGMission
{
    public class DynamicMission : ScoringMission
    {
        public string SubMissionDirectory { get; set; }
        public string MainMissionPath { get; set; }
        public int MaxNumberOfParallelRunningSubMissions { get; set; }
        public int SubMissionsLoadingInterval { get; set; }
        public int MaximumPlayerNumberPerSideForAiSpawn { get; set; }

        private List<DynamicSubMission> _subMissionList;
        private Stopwatch _lastMissionLoadStopwatch;
        private Stopwatch _subMissionCheckStopwatch;
        private Random _random;
        private int _totalNumberOfLoadedSubMissions;

        private List<string> _briefingsRed;
        private List<string> _briefingsBlue;
        private string _exclusiveMissionToLoad;

        private ConcurrentDictionary<string, DateTime> _aIAircraftList { get; set; }

        public DynamicMission()
        {
            MaxNumberOfParallelRunningSubMissions = 2;
            SubMissionsLoadingInterval = 30;
            MaximumPlayerNumberPerSideForAiSpawn = 15;
            DestroyAiGroundActorsWhenTaskComplete = true;

            _aIAircraftList = new ConcurrentDictionary<string, DateTime>();
            _subMissionList = new List<DynamicSubMission>();
            _lastMissionLoadStopwatch = new Stopwatch();
            _subMissionCheckStopwatch = new Stopwatch();
            _random = new Random();
            _totalNumberOfLoadedSubMissions = 0;

            _briefingsRed = new List<string>();
            _briefingsBlue = new List<string>();
            _exclusiveMissionToLoad = "";
        }

        public override void Init(ABattle battle, int missionNumber)
        {
            base.Init(battle, missionNumber);

            RandomBasicMissionParameterGenerator rwg = new RandomBasicMissionParameterGenerator();
            rwg.MissionFilePath = Path.Combine(CloDMyDocumentsFolder, MainMissionPath);
            rwg.MaxMissionLength = MissionLength;

            rwg.Generate();
        }

        public override void OnBattleStarted()
        {
            base.OnBattleStarted();
            ParseSubMissions();

            _subMissionCheckStopwatch.Start();

        }

        public override void OnTickGame()
        {
            base.OnTickGame();

            if (_subMissionCheckStopwatch.Elapsed.TotalSeconds >= 30)
            {
                CheckSubMissions();
                _subMissionCheckStopwatch.Restart();
            }
        }

        protected override void WritePlayerWelcomeMessage(Player player)
        {
            Message(To.Hud, player, "Welcome to the Air Combat Group server");
            Timeout(5, () => Message(To.Hud, player, "This is a dynamic mission"));
            Timeout(8, () => Message(To.Hud, player, "Check the mission briefing for current tasks"));
            Timeout(11, () => Message(To.Hud, player, "The briefing updates periodically during the mission"));
            Timeout(14, () => Message(To.Hud, player, ""));
        }

        private void ParseSubMissions()
        {
            DynamicSubMissionParser parser = new DynamicSubMissionParser(this);

            string dirPath = Path.Combine(CloDMyDocumentsFolder, SubMissionDirectory);
            string[] subMissionFiles = Directory.GetFiles(dirPath, "*.dynmis");
            foreach (string f in subMissionFiles)
            {
                _subMissionList.Add(parser.parseFile(f));
            }
        }

        private void CheckSubMissions()
        {
            bool somethingHasChanged = false;
            int runningSubMissions = 0;

            foreach (DynamicSubMission sm in _subMissionList)
            {
                if (sm.IsRunning())
                {
                    if (sm.IsFinished())
                    {
                        foreach (int btId in sm.BombTargetIDs)
                            _scoringSystem.RemoveBombTarget(btId);

                        sm.StopRunning();
                        somethingHasChanged = true;
                    }
                    else
                    {
                        runningSubMissions++;
                    }

                    //Call Retire Logic
                    RetireAircraft();
                    //RetireFighters();  Could this be cause of weirdness?  Rem out for now

                }
            }

            if (_exclusiveMissionToLoad.Length > 0)
            {
                foreach (DynamicSubMission sm in _subMissionList)
                {
                    if (sm.Exclusive && !sm.IsRunning() && sm.FileName == _exclusiveMissionToLoad)
                    {
                        Console.WriteLine("Loading Exclusive Mssion: " + _exclusiveMissionToLoad);
                        LoadSubMission(sm);
                        somethingHasChanged = true;
                        break;
                    }
                }

                _exclusiveMissionToLoad = "";
            }
            else if (runningSubMissions == 0 ||
                      (runningSubMissions < MaxNumberOfParallelRunningSubMissions &&
                       _lastMissionLoadStopwatch.Elapsed.TotalMinutes >= SubMissionsLoadingInterval))
            {
                if (LoadRandomSubMission())
                    somethingHasChanged = true;
            }

            if (somethingHasChanged)
            {
                UpdateBriefings();
                Message(To.HudAndLog, For.All, "Mission details have been updated!");
            }
        }


        public override void OnActorCreated(int missionNumber, string shortName, AiActor actor)
        {
            base.OnActorCreated(missionNumber, shortName, actor);
            if (actor is AiAircraft)
            {
                AiAircraft ac = (AiAircraft)actor;
                if (ac != null)
                {
                    if (IsAiActor(ac) == true)
                    {
                        _aIAircraftList.AddOrUpdate(actor.Name(), DateTime.Now, (k, v) => DateTime.Now);
                        AiActor byname = GamePlay.gpActorByName(actor.Name());
                    }
                }
            }
        }

        private bool LoadRandomSubMission()
        {
            bool ok = false;
            int smCount = _subMissionList.Count;
            int runCountLimit = _totalNumberOfLoadedSubMissions / smCount;
            int tryCount = 0;
            int maxTryCount = smCount * 3;

            while (!ok && tryCount < maxTryCount)
            {
                int i = _random.Next(0, smCount);
                DynamicSubMission sm = _subMissionList[i];
                if (!sm.IsRunning() && !sm.Exclusive && sm.RunCount <= runCountLimit)
                {
                    LoadSubMission(sm);
                    ok = true;
                }

                tryCount++;
            }

            return ok;
        }

        private void LoadSubMission(DynamicSubMission sm)
        {
            if (sm.MissionCommon != null)
                GamePlay.gpPostMissionLoad(sm.MissionCommon);

            if (sm.MissionAiBombersBlue != null)
                GamePlay.gpPostMissionLoad(sm.MissionAiBombersBlue);

            if (sm.MissionAiBombersRed != null)
                GamePlay.gpPostMissionLoad(sm.MissionAiBombersRed);

            if (sm.MissionAiFightersBlue != null &&
                GetArmyPlayers(For.Blue).Length <= MaximumPlayerNumberPerSideForAiSpawn)
                GamePlay.gpPostMissionLoad(sm.MissionAiFightersBlue);

            if (sm.MissionAiFightersRed != null &&
                GetArmyPlayers(For.Red).Length <= MaximumPlayerNumberPerSideForAiSpawn)
                GamePlay.gpPostMissionLoad(sm.MissionAiFightersRed);

            List<int> bombTargetIDs = new List<int>();
            if (sm.BombTargetsRed != null)
            {
                foreach (var bt in sm.BombTargetsRed)
                    bombTargetIDs.Add(_scoringSystem.AddBombTarget(bt));
            }

            if (sm.BombTargetsBlue != null)
            {
                foreach (var bt in sm.BombTargetsBlue)
                    bombTargetIDs.Add(_scoringSystem.AddBombTarget(bt));
            }


            sm.StartRunning(GetGameTime(), bombTargetIDs);
            _lastMissionLoadStopwatch.Restart();
            _totalNumberOfLoadedSubMissions++;
        }

        private void UpdateBriefings()
        {
            SortedDictionary<DateTime, DynamicSubMission> sortedRunningMissions = new SortedDictionary<DateTime, DynamicSubMission>();
            foreach (DynamicSubMission sm in _subMissionList)
            {
                if (sm.IsRunning())
                {
                    sortedRunningMissions.Add(sm.StartTime, sm);
                }
            }


            List<string> briefingsRed = new List<string>();
            List<string> briefingsBlue = new List<string>();

            string briefingRed = "";
            string briefingBlue = "";

            foreach (KeyValuePair<DateTime, DynamicSubMission> entry in sortedRunningMissions)
            {
                string briefing = entry.Value.GetBriefing(Army.Red);
                if (briefing != null)
                {
                    if (!briefingsRed.Contains(briefing))
                    {
                        briefingsRed.Add(briefing);
                        briefingRed += "\n" + briefing;
                    }
                }

                briefing = entry.Value.GetBriefing(Army.Blue);
                if (briefing != null)
                {
                    if (!briefingsBlue.Contains(briefing))
                    {
                        briefingsBlue.Add(briefing);
                        briefingBlue += "\n" + briefing;
                    }
                }
            }

            _briefingsRed = briefingsRed;
            _briefingsBlue = briefingsBlue;

            UpdateInGameBriefing();
        }

        private void UpdateInGameBriefing()
        {
            Func<List<string>, string> briefingStr = delegate (List<string> l)
             {
                 string result = "";
                 int i = 1;

                 foreach (string s in l)
                 {
                     if (l.Count > 1)
                         result += String.Format("#{0}:\n", i);

                     result += s + "\n";
                     i++;
                 }

                 if (result.Length == 0)
                     result = "none\n";

                 return result;
             };

            string briefingHeader =
                "This is a long running, dynamic mission which will be different any time you fly it.\n" +
                "Your current tasks are:\n";

            string briefingFooter = String.Format(
                "- Type <score into the chat to display the current score.\n" +
                "- Detection and vectoring of aircraft groups can be done via the in game mission menu (Tab > 4 > 1).\n" +
                "- Want to fly a bomber? No problem! All targets for the AI bombers, as stated in the current mission tasks, are also valid targets for human pilots to score some points as long as the task is listed in the briefing.\n" +
                "\n" +
                "Join us on our public Teamspeak 3 server: {0}\n" +
                "{1}\n",
                Constants.AcgTeamSpeak, Constants.AcgWebsite);

            string briefingText = String.Format(
                "[Army None]\n" +
                "<Description>\n" +
                "Welcome to the Air Combat Group Server!\n" +
                "{1}\n" +
                "[Army Red]\n" +
                "<Name>\n" +
                "RAF\n" +
                "<Description>\n" +
                "{0}\n" +
                "{2}" +
                "-------------------------------\n" +
                "\n" +
                "{1}\n" +
                "[Army Blue]\n" +
                "<Name>\n" +
                "Luftwaffe\n" +
                "<Description>\n" +
                "{0}\n" +
                "{3}" +
                "-------------------------------\n" +
                "\n" +
                "{1}\n",
                briefingHeader, briefingFooter, briefingStr(_briefingsRed), briefingStr(_briefingsBlue));


            string baseFileName = Path.Combine(CloDMyDocumentsFolder, SubMissionDirectory) + "/briefing";
            string briefingFileName = baseFileName + ".briefing";
            string misFileName = baseFileName + ".mis";
            int tryCount = 0;
            bool ok = false;

            do
            {
                try
                {
                    tryCount++;
                    StreamWriter sw = new StreamWriter(misFileName, false);
                    sw.Write("");
                    sw.Close();

                    sw = new StreamWriter(briefingFileName, false);
                    sw.Write(briefingText);
                    sw.Close();

                    GamePlay.gpPostMissionLoad(misFileName);
                    ok = true;
                }
                catch (Exception e)
                {
                    string s = e.ToString();
                }

            } while (!ok && tryCount < 3);
        }


        protected override void ChatMessageWithCommandReceived(IPlayer player, string command, List<string> args)
        {
            base.ChatMessageWithCommandReceived(player, command, args);

            if (command == "briefing" || command == "brief")
            {
                SendBriefingToPlayerViaChat(player);
            }
            else if (player == GamePlay.gpPlayer() && command == "loadmis" && args.Count > 0)
            {
                _exclusiveMissionToLoad = args[0];
                Console.WriteLine("Exclusive Mission Load Requested: " + _exclusiveMissionToLoad);
            }

        }

        public void SendBriefingToPlayerViaChat(IPlayer player)
        {
            List<string> briefings = null;
            Army army = (Army)player.Army();

            if (army == Army.Red)
                briefings = _briefingsRed;
            else if (army == Army.Blue)
                briefings = _briefingsBlue;

            if (briefings == null)
                return;

            if (briefings.Count == 0)
            {
                Message(To.Log, player, "No briefing available");
                return;
            }

            int maxMessageLength = 250; // Maximum possible text length which can be send to the player chat
            int i = 1;
            foreach (string briefing in briefings)
            {
                string text = String.Format("[Briefing #{0}]\n{1}", i, briefing);
                int offset = 0;
                while (offset < text.Length)
                {
                    int size = Math.Min(maxMessageLength, text.Length - offset);
                    Message(To.Log, player, text.Substring(offset, size));
                    offset += size;
                }

                ++i;
            }
        }
        
        public void RetireAircraft(int retireTime = 60, int retireDistance = 20000)
        {
            List<string> actorNames = new List<string>();
            actorNames = _aIAircraftList.Keys.ToList();
            foreach (string actorName in actorNames)
            {
                if ((DateTime.Now - _aIAircraftList[actorName]).TotalMinutes >= retireTime)
                {
                    AiActor actor = GamePlay.gpActorByName(actorName);
                    if (actor != null)
                    {
                        List<Tuple<AiAircraft, double>> acInRange = FindAircraftInRange(actor.Pos(), retireDistance, true);
                        if (acInRange.Count == 0)
                        {
                            //Do not destroy any ac that has damaged a ground/static?  This is a guess to stop the log flood that
                            //seems to comes when occasionally destroying AI aircraft.
                            if (isGroundDamager(actor) == false)
                            {
                                Retire(actor);
                            }
                        }
                    }
                }
            }
        }

        public bool isGroundDamager(AiActor actor)
        {
            bool actorIsGroundDamager = false;
            try
            {
                foreach (Army army in Enum.GetValues(typeof(Army)))
                {
                    AiGroundGroup[] gGroups = GamePlay.gpGroundGroups((int)army);
                    if (gGroups != null)
                    {
                        foreach (AiActor a in gGroups)
                        {
                            if (a != null)
                            {
                                foreach (DamagerScore di in this.Battle.GetDamageInitiators(a))
                                {
                                    if (di.initiator.Actor == actor)
                                    {
                                        actorIsGroundDamager = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                return actorIsGroundDamager;
            } catch (Exception e)
            {
                Message(To.Log, "ERROR IN isGroundDamager");
                Message(To.Log, e.Message);
                Message(To.Log, e.InnerException.ToString());
                return false;
            }
        }


        public void RetireFighters(int retireNum = 10, int retireDistance = 20000)
        {
            List<AiAircraft> blueResults = new List<AiAircraft>();
            List<AiAircraft> redResults = new List<AiAircraft>();
            List<AiAircraft> aiFighters = new List<AiAircraft>();
            List<AiAircraft> humans = new List<AiAircraft>();
            List<AiAirGroup> airGroups = GetAllAirGroups();
            int blueFighters = 0;
            int redFighters = 0;
            foreach (AiAirGroup airGroup in airGroups)
            {
                foreach (AiActor actor in airGroup.GetItems())
                {
                    AiAircraft aircraft = (actor as AiAircraft);
                    if (aircraft != null && aircraft.IsAlive())
                    {
                        if (aircraft.Type() == AircraftType.Fighter)
                        {
                            if (IsAiActor(actor) == false)
                            {
                                humans.Add(aircraft);
                                if (aircraft.Army() == 1)
                                {
                                    redFighters++;
                                }
                                if (aircraft.Army() == 2)
                                {
                                    blueFighters++;
                                }
                            }
                            else
                            {
                                aiFighters.Add(aircraft);
                            }
                        }
                        else
                        {
                            if (IsAiActor(actor) == false)
                            {
                                humans.Add(aircraft);
                            }
                        }
                    }
                }
            }

            //We now have a list of AI Fighters and a list of Human Aircraft.  Check range
            foreach (AiAircraft ai in aiFighters)
            {
                Point3d aiPos = ai.Pos();
                bool canRetire = true;
                foreach (AiAircraft human in humans)
                {
                    if (human.Pos().distance(ref aiPos) < retireDistance)
                    {
                        canRetire = false;
                    }
                }
                if (canRetire == true)
                {
                    if (ai.Army() == 1)
                    {
                        redResults.Add(ai);
                    }
                    if (ai.Army() == 2)
                    {
                        blueResults.Add(ai);
                    }
                }
            }
            //Do we have reason to retire?
            if (redFighters >= retireNum)
            {
                //Retire them
                foreach (AiAircraft fighter in redResults)
                {
                    Retire(fighter);
                }
            }
            if (blueFighters >= retireNum)
            {
                //Retire them
                foreach (AiAircraft fighter in blueResults)
                {
                    Retire(fighter);
                }
            }
        }
        public void Retire(AiActor aircraft)
        {
            //Message(To.Log, "Retiring " + aircraft.Name());
            DestroyActor(aircraft);
            DateTime removedTime;
            _aIAircraftList.TryRemove(aircraft.Name(), out removedTime);
        }
    }
}