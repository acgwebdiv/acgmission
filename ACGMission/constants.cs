﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace ACGMission
{
    public struct Constants
    {
        public static CultureInfo Culture = new CultureInfo("EN-us");
        public static string AcgWebsite = "www.aircombatgroup.co.uk";
        public static string AcgTeamSpeak = "85.236.100.27:16317";
    }
}
