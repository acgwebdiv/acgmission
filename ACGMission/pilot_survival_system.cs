﻿using maddox.game;
using maddox.game.world;
using maddox.GP;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace ACGMission
{
    public class PilotSurvivalSystem
    {
        public class ChancesSettings
        {
            ///<summary>Defines the chance (in percentage 0.00 - 1.00) of returning to own lines after landing or bail out at enemy land territory</summary>
            public double ReturnFromEnemyLandTerritoryChance { get; set; }
            ///<summary>Defines the chance (in percentage 0.00 - 1.00) of returning to own lines after landing or bail out at neutral land territory</summary>
            public double ReturnFromNeutralLandTerritoryChance { get; set; }

            ///<summary>Defines the chance (in percentage 0.00 - 1.00) of escaping from POW camp</summary>
            public double PowEscapeChance { get; set; }

            ///<summary>Defines the chance (in percentage 0.00 - 1.00) of being rescued by friendlies after landing or bail out at friendly water territory</summary>
            public double FriendlyAsrChanceFriendlyTerritory { get; set; }
            ///<summary>Defines the chance (in percentage 0.00 - 1.00) of being rescued by friendlies after landing or bail out at enemy water territory</summary>
            public double FriendlyAsrChanceEnemyTerritory { get; set; }
            ///<summary>Defines the chance (in percentage 0.00 - 1.00) of being rescued by friendlies after landing or bail out at neutral water territory</summary>
            public double FriendlyAsrChanceNeutralTerritory { get; set; }
            ///<summary>Defines the comradeship bonus (in percentage 0.00 - 1.00) for being rescued from any water territory</summary>
            public double FriendlyAsrChanceComradeshipBonus { get; set; }
            ///<summary>Defines the range (in meters) for the friendly ASR comradeship bonus </summary>
            public double FriendlyAsrComradeshipBonusRange { get; set; }

            ///<summary>Defines the chance (in percentage 0.00 - 1.00) of being rescued by enemies after landing or bail out at friendly water territory</summary>
            public double EnemyAsrChanceFriendlyTerritory { get; set; }
            ///<summary>Defines the chance (in percentage 0.00 - 1.00) of being rescued by enemies after landing or bail out at enemy water territory</summary>
            public double EnemyAsrChanceEnemyTerritory { get; set; }
            ///<summary>Defines the chance (in percentage 0.00 - 1.00) of being rescued by enemies after landing or bail out at neutral water territory</summary>
            public double EnemyAsrChanceNeutralTerritory { get; set; }

            public ChancesSettings()
            {
                ReturnFromEnemyLandTerritoryChance = 0.00;
                ReturnFromNeutralLandTerritoryChance = 0.00;
                PowEscapeChance = 0.00;
                FriendlyAsrChanceFriendlyTerritory = 0.00;
                FriendlyAsrChanceEnemyTerritory = 0.00;
                FriendlyAsrChanceNeutralTerritory = 0.00;
                FriendlyAsrChanceComradeshipBonus = 0.00;
                FriendlyAsrComradeshipBonusRange = 0.00;
                EnemyAsrChanceFriendlyTerritory = 0.00;
                EnemyAsrChanceEnemyTerritory = 0.00;
                EnemyAsrChanceNeutralTerritory = 0.00;
            }
        }

        private BaseMission _mission;
        private Random _random;
        private ConcurrentDictionary<string, float> _pilotHealth;

        public ChancesSettings SettingsBlue { get; set; }
        public ChancesSettings SettingsRed { get; set; }

        private enum LandingType
        {
            Landed,
            CrashLanded,
            Parachuted,
            ParachuteFailed
        }

        private enum TerritoryType
        {
            Neutral,
            Friendly,
            Enemy
        }

        private class Chances
        {
            public double ReturnFromLandTerritory { get; set; }
            public double PowEscape { get; set; }
            public double FriendlyAsrRescue { get; set; }
            public double FriendlyAsrRescueWithComradeshipBonus { get; set; }
            public double FriendlyAsrRescueComradeshipBonusRange { get; set; }
            public double EnemyAsrRescue { get; set; }

            public Chances()
            {
                ReturnFromLandTerritory = 0.00;
                PowEscape = 0.00;
                FriendlyAsrRescue = 0.00;
                FriendlyAsrRescueWithComradeshipBonus = 0.00;
                FriendlyAsrRescueComradeshipBonusRange = 0.00;
                EnemyAsrRescue = 0.00;
            }
        }

        public PilotSurvivalSystem(BaseMission mission)
        {
            _mission = mission;
            _random = new Random();
            _pilotHealth = new ConcurrentDictionary<string, float>();

            SettingsBlue = new ChancesSettings();
            SettingsBlue.ReturnFromEnemyLandTerritoryChance = 0.02;
            SettingsBlue.ReturnFromNeutralLandTerritoryChance = 0.85;
            SettingsBlue.PowEscapeChance = 0.01;
            SettingsBlue.FriendlyAsrChanceFriendlyTerritory = 0.90;
            SettingsBlue.FriendlyAsrChanceEnemyTerritory = 0.15;
            SettingsBlue.FriendlyAsrChanceNeutralTerritory = 0.50;
            SettingsBlue.FriendlyAsrChanceComradeshipBonus = 0.20;
            SettingsBlue.FriendlyAsrComradeshipBonusRange = 2500.0;
            SettingsBlue.EnemyAsrChanceFriendlyTerritory = 0.00;
            SettingsBlue.EnemyAsrChanceEnemyTerritory = 0.10;
            SettingsBlue.EnemyAsrChanceNeutralTerritory = 0.05;

            SettingsRed = new ChancesSettings();
            SettingsRed.ReturnFromEnemyLandTerritoryChance = 0.07;
            SettingsRed.ReturnFromNeutralLandTerritoryChance = 0.85;
            SettingsRed.PowEscapeChance = 0.02;
            SettingsRed.FriendlyAsrChanceFriendlyTerritory = 0.90;
            SettingsRed.FriendlyAsrChanceEnemyTerritory = 0.08;
            SettingsRed.FriendlyAsrChanceNeutralTerritory = 0.50;
            SettingsRed.FriendlyAsrChanceComradeshipBonus = 0.20;
            SettingsRed.FriendlyAsrComradeshipBonusRange = 2500.0;
            SettingsRed.EnemyAsrChanceFriendlyTerritory = 0.00;
            SettingsRed.EnemyAsrChanceEnemyTerritory = 0.10;
            SettingsRed.EnemyAsrChanceNeutralTerritory = 0.05;
        }

        public void AircraftLanded(AiAircraft aircraft, bool crash = false)
        {
            List<Player> players = new List<Player>();
            string resultString = "";

            for (int i = 0; i < aircraft.Places(); i++)
            {
                Player player = aircraft.Player(i);

                if (player == null)
                {
                    string placeString = _mission.GenerateAircraftPlaceString(aircraft, i);
                    if (_mission.PlayerAircraftPlaces.ContainsKey(placeString))
                        player = _mission.PlayerAircraftPlaces[placeString];
                }

                if (player == null || players.Contains(player))
                    continue;

                if (crash == false)
                {
                    resultString = ProcessLanding(player, (Army)aircraft.Army(), aircraft.Pos(), LandingType.Landed);
                }
                else
                {
                    resultString = ProcessLanding(player, (Army)aircraft.Army(), aircraft.Pos(), LandingType.CrashLanded);
                }
                _mission._eventOutHandler.LogAircraftLanded(_mission.GetGameTime().ToString("HH:mm:ss"), aircraft, resultString, crash);

                players.Add(player);
            }
        }

        public void PersonParachuteLanded(AiPerson person)
        {
            if (person.Player() != null)
            {
                string result = ProcessLanding(person.Player(), (Army)person.Army(), person.Pos(), LandingType.Parachuted);
                _mission._eventOutHandler.LogPersonParachuteLanded(_mission.GetGameTime().ToString("HH:mm:ss"), person, result);
            }
        }

        public void PersonParachuteFailed(AiPerson person)
        {
            if (person.Player() != null)
            {
                string result = ProcessLanding(person.Player(), (Army)person.Army(), person.Pos(), LandingType.ParachuteFailed);
                _mission._eventOutHandler.LogPersonParachuteFailed(_mission.GetGameTime().ToString("HH:mm:ss"), person, result);
            }
        }

        public void OnPersonHealth(AiPerson person, AiDamageInitiator initiator, float deltaHealth)
        {
            if (person != null)
            {
                Player player = person.Player();
                if (player != null)
                {
                    _pilotHealth.AddOrUpdate(player.Name(), person.Health, (k, v) => person.Health);
                }
            }
        }

        private bool succumbedToWounds(float health, float modifier = 0f)
        {
            bool retval = false;
            health = health - modifier;
            if ((health) <= 0.5)
            {
                if (_random.NextDouble() < (1 - health))
                {
                    retval = true;
                }
            }
            return retval;
        }

        private string ProcessLanding(Player player, Army army, Point3d pos, LandingType landingType)
        {
            string playerName = player.Name();
            Army armyAtPos = _mission.ArmyAtPos(pos);
            TerritoryType territoryType = DetermineTerritoryType(army, armyAtPos);
            Chances chances = DetermineChances(army, territoryType);
            //bool landedInWater = _mission.GamePlay.gpLandType(pos.x, pos.y) == LandTypes.WATER;
            bool landedInWater = _mission.LandTypeAtPos(pos) == LandTypes.WATER;

            float health;
            if (!_pilotHealth.TryGetValue(playerName, out health))
            {
                health = 1.0f;
            }

            // Think about: GamePlay.gpFrontDistance

            string msgGlobal = "";
            string msgPlayer = "";

            /*if (health < 0.4f) {
                msgGlobal = String.Format("{0} is badly injured.  Their health on landing is {1}. ", playerName, health.ToString());
                msgPlayer = String.Format("You are badly injured.  Your health on landing is {0}. ", health.ToString());
            }*/

            bool messageComplete = false;
            int PAMStatus = 1;

            string tw = "territory";
            if(landedInWater)
            {
                tw = "waters";
            }

            switch (landingType)
            {
                case LandingType.Landed:
                    msgGlobal += String.Format("{0} landed in {1} {2}", playerName, territoryType.ToString().ToLower(), tw);
                    msgPlayer += String.Format("You landed in {0} {1}", territoryType.ToString().ToLower(), tw);
                    /*if (succumbedToWounds(health) == true)
                    {
                        msgGlobal += " but died injuries sustained in the flight.";
                        msgPlayer += " but died injuries sustained in the flight.";
                        PAMStatus = 3;
                        messageComplete = true;
                    }*/
                    break;
                case LandingType.CrashLanded:
                    msgGlobal += String.Format("{0} crash landed in {1} {2}", playerName, territoryType.ToString().ToLower(), tw);
                    msgPlayer += String.Format("You crash landed in {0} {1}", territoryType.ToString().ToLower(), tw);
                    /*if (succumbedToWounds(health,0.05f) == true)
                    {
                        msgGlobal += " but died injuries sustained in the flight.";
                        msgPlayer += " but died injuries sustained in the flight.";
                        PAMStatus = 3;
                        messageComplete = true;
                    }*/
                    break;
                case LandingType.Parachuted:
                    msgGlobal += String.Format("{0} parachuted into {1} {2}", playerName, territoryType.ToString().ToLower(), tw);
                    msgPlayer += String.Format("You parachuted into {0} {1}", territoryType.ToString().ToLower(), tw);
                    /*if (succumbedToWounds(health,0.05f) == true)
                    {
                        msgGlobal += " but died injuries sustained in the flight.";
                        msgPlayer += " but died injuries sustained in the flight.";
                        PAMStatus = 3;
                        messageComplete = true;
                    }*/
                    break;
                case LandingType.ParachuteFailed:
                    msgGlobal += String.Format("{0} fell to death in {1} {2} after a parachute failure", playerName, territoryType.ToString().ToLower(), tw);
                    msgPlayer += String.Format("You fell to death in {0} {1} after a parachute failure", territoryType.ToString().ToLower(), tw);
                    PAMStatus = 3;
                    messageComplete = true;
                    break;
            }

            if (!messageComplete)
            {
                if (landedInWater)
                {
                    double friendlyAsrRescue = chances.FriendlyAsrRescue;
                    bool comradeshipBonus = false;
                    if (DetermineFriendlyAsrComradeshipBonus(player, army, pos, chances.FriendlyAsrRescueComradeshipBonusRange))
                    {
                        friendlyAsrRescue = chances.FriendlyAsrRescueWithComradeshipBonus;
                        comradeshipBonus = true;
                    }

                    if (_random.NextDouble() > (1 - friendlyAsrRescue))
                    {
                        if (comradeshipBonus)
                        {
                            msgGlobal += " and was rescued by friendly ASR with help from his comrades";
                            msgPlayer += " and were rescued by friendly ASR with help from your comrades";
                        }
                        else
                        {
                            msgGlobal += " and was rescued by friendly ASR";
                            msgPlayer += " and were rescued by friendly ASR";
                        }
                    }
                    else if (_random.NextDouble() > (1 - chances.EnemyAsrRescue))
                    {
                        if (_random.NextDouble() > (1 - chances.PowEscape))
                        {
                            msgGlobal += " and was captured by enemy ASR but managed to escape from the POW camp";
                            msgPlayer += " and were captured by enemy ASR but managed to escape from the POW camp";
                        }
                        else
                        {
                            msgGlobal += " and was captured by enemy ASR";
                            msgPlayer += " and were captured by enemy ASR";
                            PAMStatus = 2;
                        }
                    }
                    else
                    {
                        msgGlobal += " and drowned";
                        msgPlayer += " and drowned";
                        PAMStatus = 3;
                    }
                }
                else if (territoryType != TerritoryType.Friendly)
                {
                    if (_random.NextDouble() > (1 - chances.ReturnFromLandTerritory))
                    {
                        if (territoryType == TerritoryType.Neutral)
                        {
                            msgGlobal += " and returned to friendly lines";
                            msgPlayer += " and returned to friendly lines";
                        }
                        else if (territoryType == TerritoryType.Enemy)
                        {
                            msgGlobal += " but escaped capture and returned to friendly lines";
                            msgPlayer += " but escaped capture and returned to friendly lines";
                        }
                    }
                    else
                    {
                        if (territoryType == TerritoryType.Neutral)
                        {
                            msgGlobal += " but failed to return to friendly lines";
                            msgPlayer += " but failed to return to friendly lines";
                            PAMStatus = 3;
                        }
                        else if (territoryType == TerritoryType.Enemy)
                        {
                            if (_random.NextDouble() > (1 - chances.PowEscape))
                            {
                                msgGlobal += " and was captured but managed to escape from the POW camp";
                                msgPlayer += " and were captured but managed to escape from the POW camp";
                            }
                            else
                            {
                                msgGlobal += " and was captured";
                                msgPlayer += " and were captured";
                                PAMStatus = 2;
                            }
                        }
                    }
                }
            }

            msgGlobal += ".";
            msgPlayer += ".";

            _mission.Message(To.Log, msgGlobal);

            if (player.IsConnected())
            {
                _mission.Message(To.Hud, player, msgPlayer);
                _mission.Timeout(5, () => _mission.Message(To.Hud, player, msgPlayer));
                _mission.Timeout(10, () => _mission.Message(To.Hud, player, msgPlayer));
            }

            return msgGlobal + "|" + PAMStatus.ToString();
        }

        private bool DetermineFriendlyAsrComradeshipBonus(Player player, Army army, Point3d pos, double range)
        {
            List<Tuple<AiAircraft, double>> aircarftsInRange = _mission.FindAircraftInRange(pos, range);
            foreach (Tuple<AiAircraft, double> entry in aircarftsInRange)
            {
                AiAircraft aircraft = entry.Item1;

                if ((Army)aircraft.Army() != army)
                    continue;

                if (aircraft.Player(0) == null)
                    continue;

                if (aircraft.Player(0) == player)
                    continue;

                return true;
            }

            return false;
        }

        private TerritoryType DetermineTerritoryType(Army army, Army armyOfTerritory)
        {
            TerritoryType result = TerritoryType.Neutral;

            if (army == armyOfTerritory)
                result = TerritoryType.Friendly;
            else if (armyOfTerritory != Army.None)
                result = TerritoryType.Enemy;

            return result;
        }

        private Chances DetermineChances(Army army, TerritoryType territoryType)
        {
            Chances result = new Chances();
            ChancesSettings settings = null;

            if (army == Army.Blue)
                settings = SettingsBlue;
            else if (army == Army.Red)
                settings = SettingsRed;
            
            if (settings != null)
            {
                result.PowEscape = settings.PowEscapeChance;

                switch (territoryType)
                {
                    case TerritoryType.Friendly:
                        result.ReturnFromLandTerritory = 1.00;
                        result.FriendlyAsrRescue = settings.FriendlyAsrChanceFriendlyTerritory;
                        result.EnemyAsrRescue = settings.EnemyAsrChanceFriendlyTerritory;
                        break;
                    case TerritoryType.Enemy:
                        result.ReturnFromLandTerritory = settings.ReturnFromEnemyLandTerritoryChance;
                        result.FriendlyAsrRescue = settings.FriendlyAsrChanceEnemyTerritory;
                        result.EnemyAsrRescue = settings.EnemyAsrChanceEnemyTerritory;
                        break;
                    case TerritoryType.Neutral:
                        result.ReturnFromLandTerritory = settings.ReturnFromNeutralLandTerritoryChance;
                        result.FriendlyAsrRescue = settings.FriendlyAsrChanceNeutralTerritory;
                        result.EnemyAsrRescue = settings.EnemyAsrChanceNeutralTerritory;
                        break;
                }

                result.FriendlyAsrRescueWithComradeshipBonus = Math.Min( 0.95, result.FriendlyAsrRescue + settings.FriendlyAsrChanceComradeshipBonus );
                result.FriendlyAsrRescueComradeshipBonusRange = settings.FriendlyAsrComradeshipBonusRange;
            }

            return result;
        }

    }
}