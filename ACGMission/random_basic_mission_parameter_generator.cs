using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace ACGMission
{
    public class RandomBasicMissionParameterGenerator
    {
        public class Map
        {
            public enum MapName
            {
                English_Channel_1940_Summer,
                English_Channel_1940_Autumn,
                English_Channel_1940_Winter
            };

            private static Dictionary<MapName,string> _mapNameDict = new Dictionary<MapName,string>()
            {
                { MapName.English_Channel_1940_Summer, "Land$English_Channel_1940" },
                { MapName.English_Channel_1940_Autumn, "Land$English_Channel_1940_Autumn" },
                { MapName.English_Channel_1940_Winter, "Land$English_Channel_1940_Winter" }
            };

            public MapName Name { get; set; }
            public TimeSpan MinMissionStartTime { get; set; }
            public TimeSpan MaxMissionEndTime { get; set; }

            public Map(MapName name, TimeSpan minMissionStartTime, TimeSpan maxMissionEndTime)
            {
                Name = name;
                MinMissionStartTime = minMissionStartTime;
                MaxMissionEndTime = maxMissionEndTime;
            }

            public string InternalName()
            {
                return _mapNameDict[Name];
            }
        }


        public string MissionFilePath { get; set; }
        public int MaxMissionLength { get; set; }

        public List<Map> Maps = new List<Map>()
        {
            new Map(Map.MapName.English_Channel_1940_Summer, new TimeSpan(6, 0, 0) , new TimeSpan(18, 0, 0)),
            new Map(Map.MapName.English_Channel_1940_Autumn, new TimeSpan(8, 0, 0) , new TimeSpan(16, 0, 0)),
            new Map(Map.MapName.English_Channel_1940_Winter, new TimeSpan(9, 0, 0) , new TimeSpan(15, 0, 0)),
        };

        private Random _random = new Random();

        public void Generate()
        {
            if (MissionFilePath == null)
                return;

            List<Tuple<string, int, int>> key = new List<Tuple<string, int, int>>();            // list of weather line tuples
            Tuple<string, int, int> tuple;  // a tuple to hold weather line information, format is key-word; lowest integer value; highest integer value

            // map
            Map map = Maps[0]; // Only summer map, due to complaints ...  Maps[ _random.Next(0, Maps.Count)];

            // mission time
            TimeSpan maxMapTime = map.MaxMissionEndTime - map.MinMissionStartTime;
            TimeSpan startTime;

            if (MaxMissionLength >= (int)maxMapTime.TotalMinutes)
            {
                startTime = map.MinMissionStartTime;
            }
            else
            {
                int i = _random.Next(0, ((int)maxMapTime.TotalMinutes - MaxMissionLength) + 1);
                DateTime dt = new DateTime(1939, 9, 1, map.MinMissionStartTime.Hours, map.MinMissionStartTime.Minutes, 0);
                dt = dt.AddMinutes(i);
                startTime = new TimeSpan(dt.Hour, dt.Minute, 0);
            }

            // cloud height
            tuple = Tuple.Create("CloudsHeight", 500, 3000); // cloud height 1500m to 3000m
            key.Add(tuple);

            // cloud density
            // 0 = clear; 1 = light cloud; 2 = medium cloud
            tuple = Tuple.Create("WeatherIndex", 0, 2); 
            key.Add(tuple);

            // amount of wind
            /* See Beaufort wind scale http://en.wikipedia.org/wiki/Beaufort_scale#Modern_scale
            0 = calm
            1 = light breeze
            2 = light breeze
            3 = gentle breeze
            4 = moderate breeze
            5 = fresh breeze
            6 = strong breeze
            7 = high winds
            8 = gale
            9 = strong gale
            10 = storm force
             * */
            tuple = Tuple.Create("BreezeActivity", 0, 4);
            key.Add(tuple);

            //
            tuple = Tuple.Create("ThermalActivity", 0, 4);
            key.Add(tuple);

            // liklihood of wind gusts
            /*
            GustPower > 8 = highly likely
            GustPower > 6 = strong possiblility
            GustPower > 4) = moderate possibility
            GustPower > 2) = possibility
            GustPower > 0) = probable
             * */
            tuple = Tuple.Create("GustPower", 0, 5);
            key.Add(tuple);

            // maximum angle of wind gusts relative to prevailing wind direction, -45 to +45 degrees
            tuple = Tuple.Create("GustAngle", 0, 45);
            key.Add(tuple);

            // calculate wind angle in degrees from mission file arctangent vectors y, x
            // ie. the line something like "Power 0.96 -3.32 0.00" where y=0.96, x=-3.32
            // randomise arctan vectors so wind direction is variable from mission to mission
            //
            // the two-argument arc tangent function, atan2(y,x), which returns the arc tangent of y/x 
            // in the range -pi to pi radians, -180 to 180 degrees.
            // ref: https://www.eol.ucar.edu/content/wind-direction-quick-reference
            //
            double DperR = 180 / Math.PI; // radians to degrees
            double uMET = 0; // positive Umet is wind blowing to the East. 
            double vMET = 0; // positive Vmet is wind blowing to the North. This is right handed with respect to an upward +Wmet.
            double DirMET = -200; // Dirmet is the direction with respect to true north, (0=north,90=east,180=south,270=west) that the wind is coming from.
            double WindSpeed = 99;
            while (DirMET < -180 || DirMET > 180 || WindSpeed > 5.9)
            {
                // random meteorological wind coordinates, right handed with respect to an upward +Wmet.
                uMET = RandomDoubleBetween(-2, 2);  // 2 m/s max
                vMET = RandomDoubleBetween(-2, 2);  // 2 m/s max
                DirMET = Math.Atan2(-uMET, -vMET) * DperR;  // get direction wind blowing FROM -180 to +180 as in game GUI
                WindSpeed = Math.Sqrt(Math.Pow(uMET, 2) + Math.Pow(vMET, 2)); // m/s
            }

            // the actual wind angle in degrees
            Double WindAngleDegrees = DirMET;
            if (DirMET < 0) WindAngleDegrees = 180 - Math.Abs(DirMET) + 180;

            tuple = Tuple.Create("Power", 0, 1);
            key.Add(tuple);

            try
            {
                // read the mission file one line at a time, randomise the weather line parameters & write to a temporary mission file
                string tmpFileName = MissionFilePath.Replace(".mis", "_tmp.mis");
                StreamReader sr = new StreamReader(MissionFilePath);            // a reader for the mission file
                StreamWriter sw = new StreamWriter(tmpFileName, true); // a writer for the new temporary mission file
                string newMisLine;
                while ((newMisLine = sr.ReadLine()) != null)                // read mission file one line at a time
                {
                    string trimmedLine = newMisLine.TrimStart();

                    if (trimmedLine.StartsWith("MAP"))
                    {
                        newMisLine = "  MAP " + map.InternalName();
                    }
                    else if (trimmedLine.StartsWith("TIME"))
                    {
                        double t = startTime.Hours + ((double)startTime.Minutes / 60.0);
                        newMisLine = "  TIME " + t.ToString(Constants.Culture);
                    }
                    else
                    {
                        for (int i = 0; i < key.Count; i++)                     // cycle through each of the weather section line key words
                        {
                            if (trimmedLine.StartsWith(key[i].Item1))   // we found a line with a weather key word
                            {
                                // generate a random integer between the low & high range (inclusive)
                                int randomInt = _random.Next(key[i].Item2, key[i].Item3 + 1);
                                //int RandomInt = _random.Next(key[i].Item3 + 1);
                                //while (RandomInt < key[i].Item2) RandomInt = _random.Next(key[i].Item3 + 1);

                                // look for some special weather words & process them seperately
                                if (newMisLine.Contains("GustAngle") && _random.Next(100) > 50)
                                    randomInt = -randomInt;  // gustangle can be + or -

                                if (newMisLine.TrimStart().StartsWith("Power"))
                                {
                                    newMisLine = "  " + key[i].Item1 + " " + uMET.ToString("0.000", Constants.Culture) + " " + vMET.ToString("0.000", Constants.Culture) + " " + "0.000"; // power vector line
                                }
                                else
                                {
                                    newMisLine = "  " + key[i].Item1 + " " + randomInt.ToString();  // use the random integer value
                                }

                                break;
                            }
                        }
                    }
                    sw.WriteLine(newMisLine);
                }
                sr.Close();     // close the mission file to processing 
                sw.Close();     // close temporary mission file tmpWeather
                File.Replace(tmpFileName, MissionFilePath, null);  // rename temporary mis file (tmpWeather) as the mission file (overwrites mis file)
            }
            catch (Exception e)
            {
                string s = e.ToString();
            }
        }

        private double RandomDoubleBetween(double fMin, double fMax)
        {
            double result = _random.NextDouble() * (fMax - fMin) + fMin;
            return result;
        }
    }
}