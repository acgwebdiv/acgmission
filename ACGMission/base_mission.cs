﻿using ACGMission.ExternalOutput;
using maddox.game;
using maddox.game.world;
using maddox.GP;
using part;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;

namespace ACGMission
{

    public class BaseMission : XMission
    {
        #region Mission Parameters
        public int MissionLength { get; set; }
        public int ReoccuringMessageFrequence { get; set; }
        public int MissionStatusMessageFrequence { get; set; }
        public bool PilotSurvivalSystemEnabled { get; set; }
        public bool DestroyAiWhenTaskComplete { get; set; }
        public bool DestroyAiGroundActorsWhenTaskComplete { get; set; }
        #endregion

        public string CloDMyDocumentsFolder { get; private set; }
        public Dictionary<string, Player> PlayerAircraftPlaces { get; private set; }

        Stopwatch _swMissionEnd = new Stopwatch();
        Stopwatch _swReoccuringMessage = new Stopwatch();
        Stopwatch _swMissionStatusMessage = new Stopwatch();
        Stopwatch _swCleanup = new Stopwatch();
        Stopwatch _swEventOut = new Stopwatch();

        List<Player> _playersShowedWelcomeMessage = new List<Player>();

        public PilotSurvivalSystem _pilotSurvivalSystem;
        public eventOutHandler _eventOutHandler;

        private ExternalOutputController _externalOutputController;
        private string missionStartTime;
        public bool _amCAMPAIGNMission = false;


        public int DATAPORT = 50000;
        public int BOMBERSAWAYPORT = 50001;
        public int DESPAWNPORT = 50002;
        public bool Il2xEnabled = true;
        public string pathToEventOutBaseFolder = "C:\\Users\\Administrator\\Dropbox\\Shared\\EventOut";

        

        public BaseMission()
        {
            //CloDMyDocumentsFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "1C SoftClub\\il-2 sturmovik cliffs of dover");
            //NOTE, the "Folderpath" is set in xmission.  It is the path to the 1c mydocs folder.
            CloDMyDocumentsFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), this.docFolderPath);

            PlayerAircraftPlaces = new Dictionary<string, Player>();

            MissionLength = 60;

            ReoccuringMessageFrequence = 6;
            MissionStatusMessageFrequence = 5;

            PilotSurvivalSystemEnabled = true;
            DestroyAiWhenTaskComplete = true;
            DestroyAiGroundActorsWhenTaskComplete = false;

            _pilotSurvivalSystem = new PilotSurvivalSystem(this);
            

            missionStartTime = DateTime.Now.ToString("yyyyMMddHHmmss");
            Console.WriteLine("MISSION-START-TIME:" + missionStartTime);
        }

        public override void OnBattleInit()
        {
            base.OnBattleInit();
            
            /*
             * To enable the chat parsing it is necessary that the script runs in the defualt appdomain.
             * Activate this by inserting the following entry into the conf.ini and confs.ini:
             *   [rts]
             *   scriptAppDomain=0
            */ 
            if (AppDomain.CurrentDomain.IsDefaultAppDomain())
            {
                if (GamePlay is GameDef)
                {
                    (GamePlay as GameDef).EventChat += new GameDef.Chat(ChatMessageReceived);
                }
            }
        }

        public override void OnBattleStarted()
        {
            base.OnBattleStarted();

            Console.WriteLine("IL2X enabled : " + Il2xEnabled.ToString());
            Console.WriteLine("DATAPORT : " + DATAPORT.ToString());
            Console.WriteLine("BOMBERSAWAYPORT : " + BOMBERSAWAYPORT.ToString());
            Console.WriteLine("DESPAWNPORT : " + DESPAWNPORT.ToString());
            Console.WriteLine("pathToEventOutBaseFolder : " + pathToEventOutBaseFolder.ToString());
            Console.WriteLine("Campaign Mission : " + _amCAMPAIGNMission.ToString());
            Console.WriteLine();
            _eventOutHandler = new eventOutHandler(pathToEventOutBaseFolder, _amCAMPAIGNMission);
            _externalOutputController = new ExternalOutputController(this);

            IsLogEnabled = true;
            if (Il2xEnabled == true) { 
                RunIL2X();
            }

            _swMissionEnd.Start();
            _swReoccuringMessage.Start();
            _swMissionStatusMessage.Start();
            _swCleanup.Start();
            _swEventOut.Start();
            
        }

        public override void OnTickGame()
        {
            base.OnTickGame();

            if (IsEveryNthSecond(2))
            {
                HFProcess();
            }


            if (ReoccuringMessageFrequence > 0 && _swReoccuringMessage.Elapsed.TotalMinutes >= ReoccuringMessageFrequence)
            {
                _swReoccuringMessage.Restart();
                WriteReoccurringMessage();                
            }

            if (MissionStatusMessageFrequence > 0 && _swMissionStatusMessage.Elapsed.TotalMinutes >= MissionStatusMessageFrequence)
            {
                _swMissionStatusMessage.Restart();
                WriteMissionStatusMessage();
            }

            if (_swMissionEnd.Elapsed.TotalMinutes >= MissionLength)
            {
                _swMissionEnd.Stop();
                _swMissionEnd.Reset();
                _swReoccuringMessage.Stop();
                _swReoccuringMessage.Reset();
                _swMissionStatusMessage.Stop();
                _swMissionStatusMessage.Reset();
                _swCleanup.Stop();
                _swCleanup.Reset();

                WriteMissionEndMessage();

                Timeout(100, () => Message(To.HudAndLog, "New mission loading in 60 seconds. Please reconnect to play"));
                Timeout(160, () => Process.GetCurrentProcess().Kill());
            }

            if (_swCleanup.Elapsed.TotalSeconds >= 10)
            {
                _swCleanup.Restart();

                if (DestroyAiGroundActorsWhenTaskComplete)
                {
                    DestroyFinishedGroundUnits(GamePlay.gpGroundGroups((int)Army.Red));
                    DestroyFinishedGroundUnits(GamePlay.gpGroundGroups((int)Army.Blue));
                }

                /*
                if (DestroyAiWhenTaskComplete)
                {
                    DestroyFinishedAiAirGroups(GamePlay.gpAirGroups((int)Army.Blue));
                    DestroyFinishedAiAirGroups(GamePlay.gpAirGroups((int)Army.Red));
                }
                */
            }

            if (_swEventOut.Elapsed.TotalSeconds >= 1)
            {
                _swEventOut.Restart();
                _eventOutHandler.writeEventOut();
            }
        }
        
        private void HFProcess()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            List<string> _myKillList = _externalOutputController.killList;
            Dictionary<string, int> killDic = new Dictionary<string, int>();
            bool despawned = false;
            foreach (string id in _myKillList)
            {
                killDic.Add(id, 0);
            }
            List<string> _killedList = new List<string>();

            //Harvest all of the individual ac for tracking purposes.
            List<UniquePilot> pilotList = new List<UniquePilot>();

            Player[] playerList = GamePlay.gpRemotePlayers();
            int playersInGame = playerList.Count();

            Dictionary<string, AiWayPoint[]> AIGroupData = new Dictionary<string, AiWayPoint[]>();
            Dictionary<string, int> AICurrWP = new Dictionary<string, int>();
            var groups = GetAllAirGroups();

            

            playersInGame = 0;
            if (groups != null)
            {
                foreach (var airGroup in groups)
                {
                    //WP addition
                    AiWayPoint[] ways = null;
                    int current = -1;
                    string groupName = airGroup.Name();
                    bool groupContainsAI = false;
                    //Let us also grab their waypoints
                    try
                    {
                        ways = airGroup.GetWay();
                        current = airGroup.GetCurrentWayPoint();
                    }
                    catch
                    {
                    }
                    var items = airGroup.GetItems();
                    foreach (var actor in items)
                    {
                        try
                        {
                            var aircraft = actor as AiAircraft;

                            AiPerson person = aircraft.Person(0);
                            if (person != null)
                            {
                                string name = "";
                                string playerType = "";
                                bool proceed = false;

                                Point3d playerPosition = aircraft.Pos();
                                if (GetSectorLocation(playerPosition).ToString().IndexOf('<') == -1)
                                {
                                    if (person.Player() == null)
                                    {
                                        //AI
                                        name = person.Name();
                                        playerType = "1";
                                        proceed = true;
                                        groupContainsAI = true;
                                        //Add any change to waypoints here
                                    }
                                    else
                                    {
                                        //Breather
                                        name = person.Player().Name();
                                        playerType = "0";
                                        proceed = true;
                                        playersInGame++;
                                        groupName = "";
                                    }
                                    if (killDic.ContainsKey(name))
                                    {
                                        despawned = true;
                                        try
                                        {
                                            aircraft.Destroy();
                                            _killedList.Add(name);
                                        }
                                        catch
                                        {
                                        }
                                    }
                                }
                                if (proceed == true)
                                {
                                    UniquePilot pilot = new UniquePilot();
                                    pilot.Id = name;
                                    pilot.GrpName = groupName;
                                    pilot.IsAI = playerType;
                                    pilot.MapGrid = GetSectorLocation(playerPosition).ToString();
                                    pilot.Type = aircraft.InternalTypeName().Replace("bob:Aircraft.", "");
                                    pilot.Type = pilot.Type.Split('_')[0];
                                    pilot.Type = pilot.Type.Replace("Late", "");
                                    pilot.X = Math.Round(playerPosition.x, 0).ToString();
                                    pilot.Y = Math.Round(playerPosition.y, 0).ToString();
                                    pilot.Z = Math.Round((playerPosition.z), 0).ToString();

                                    pilot.Faction = "LW";
                                    if (actor.Army() == (int)Army.Red)
                                    {
                                        pilot.Faction = "RAF";
                                    }
                                    pilotList.Add(pilot);
                                }
                            }
                        }
                        catch
                        {
                        }
                    }

                    //Add waypoints
                    if (groupContainsAI == true)
                    {
                        if (AIGroupData.ContainsKey(groupName) == false)
                        {
                            AIGroupData.Add(groupName, ways);
                        }
                        if (AICurrWP.ContainsKey(groupName) == false)
                        {
                            AICurrWP.Add(groupName, current);
                        }
                    }
                }
            }

            if (despawned == true)
            {
                if (_killedList.Count > 0)
                {
                    foreach (string badPlayer in _killedList)
                    {
                        Message(To.Hud, badPlayer, "No flight report, no flight. You are either on the campaign blacklist or the banlist.");
                    }
                }
                _externalOutputController.setKillList(new List<string>());
            }

            _externalOutputController.setCurrentMissionData(missionStartTime, pilotList, playersInGame, AIGroupData, AICurrWP);

            sw.Stop();
        }
        

        ///<summary>Can be overriden to write a information/welcome message to the players. It is called frequently based on the given ReoccuringMessageFrequence value.</summary>
        protected virtual void WriteReoccurringMessage()
        {
            Timeout(0, () => Message(To.Log, "Welcome to the Air Combat Group server"));
            Timeout(2, () => Message(To.Log, Constants.AcgWebsite));
            Timeout(4, () => Message(To.Log, String.Format("ACG Teamspeak server: {0} (no password)", Constants.AcgTeamSpeak)));
            Timeout(6, () => Message(To.Log, "We are recruiting! Allied and Axis pilots, all standards welcome"));
        }

        ///<summary>Can be overriden to write a mission status (e.g. current score) message to the players. It is called frequently based on the given MissionStatusMessageFrequence value.</summary>
        protected virtual void WriteMissionStatusMessage()
        {
        }

        ///<summary>Can be overriden to write a message to the players on mission end. It is called automatically on mission end.</summary>
        protected virtual void WriteMissionEndMessage()
        {
        }

        ///<summary>Can be overriden to write a weclome or mssion intro message to the player. It is called once for each player when they spawn in the first time.</summary>
        protected virtual void WritePlayerWelcomeMessage(Player player)
        {
            Message(To.Hud, player, "Welcome to the Air Combat Group server");
        }

        ///<summary>Returns the remaining mission time in minutes (by default) or seconds (by parameter).</summary>
        public int RemainingMissionTime(bool inSeconds = false)
        {
            int result = 0;

            if(inSeconds)
                result = (int)((MissionLength * 60) - _swMissionEnd.Elapsed.TotalSeconds);
            else
                result = (int)(MissionLength - _swMissionEnd.Elapsed.TotalMinutes);

            return result;
        }

        public string RemainingMissionTimeOutputString()
        {
            TimeSpan ts = TimeSpan.FromSeconds(RemainingMissionTime(true));
            return String.Format("Mission Time Remaining: {0:D2}:{1:D2}:{2:D2}", ts.Hours, ts.Minutes, ts.Seconds);
        }

        protected virtual void ChatMessageReceived(IPlayer player, string chatMsg)
        {
            if (chatMsg.StartsWith("<"))
            {
                List<string> split = chatMsg.Remove(0, 1).Trim().Split(' ').ToList();

                if (split.Count > 0)
                {
                    string command = split.First().ToLower();
                    split.RemoveAt(0);
                    Timeout(1, () => ChatMessageWithCommandReceived(player, command, split));
                }
            }
        }

        protected virtual void ChatMessageWithCommandReceived(IPlayer player, string command, List<string> args)
        {
            switch (command)
            {
                case "tl":
                case "timeleft":
                    Message(To.Log, player, RemainingMissionTimeOutputString());
                    break;
                case "rr":
                    Message(To.Log, player, "Rearm and refuel isn't available");
                    break;

            }
        }



        public override void OnPlaceEnter(Player player, AiActor actor, int placeIndex)
        {
            base.OnPlaceEnter(player, actor, placeIndex);

            if (player == null)
                return;

            if (!_playersShowedWelcomeMessage.Contains(player))
            {
                _playersShowedWelcomeMessage.Add(player);
                WritePlayerWelcomeMessage(player);
            }

            AiAircraft aircraft = (actor as AiAircraft);
            if (aircraft != null)
            {
                string placeString = GenerateAircraftPlaceString(aircraft, placeIndex);
                if (!PlayerAircraftPlaces.ContainsKey(placeString))
                    PlayerAircraftPlaces.Add(placeString, player);
            }
        }

        #region  Events needed in EventOut handler not referenced elsewhere

        public override void OnPlaceLeave(Player player, AiActor actor, int placeIndex)
        {
            base.OnPlaceLeave(player, actor, placeIndex);
            Timeout(1, () =>
                {
                    DamageAiControlledPlane(actor);
                    _eventOutHandler.IsThisAJumper(GetGameTime().ToString("HH:mm:ss"), actor as AiAircraft, player);
                }
            );
        }

        public override void OnAircraftKilled(int missionNumber, string shortName, AiAircraft aircraft)
        {
            base.OnAircraftKilled(missionNumber, shortName, aircraft);
            _eventOutHandler.LogAircraftKilled(this, GetGameTime().ToString("HH:mm:ss"), aircraft);

        }
        
        public override void OnActorCreated(int missionNumber, string shortName, AiActor actor)
        {
            base.OnActorCreated(missionNumber, shortName, actor);
            Timeout(1, () =>
                { _eventOutHandler.LogActorCreatedDetail(GetGameTime().ToString("HH:mm:ss"), actor); }
            );

        }

        public override void OnPlayerConnected(Player player)
        {
            base.OnPlayerConnected(player);
            _eventOutHandler.LogPlayerConnected(GetGameTime().ToString("HH:mm:ss"), player);
        }

        public override void OnPlayerDisconnected(Player player, string diagnostic)
        {
            base.OnPlayerDisconnected(player, diagnostic);
            _eventOutHandler.LogPlayerDisconnected(GetGameTime().ToString("HH:mm:ss"), player, diagnostic);
        }

        public override void OnAircraftTookOff(int missionNumber, string shortName, AiAircraft aircraft)
        {
            base.OnAircraftTookOff(missionNumber, shortName, aircraft);
            _eventOutHandler.LogAircraftTookOff(GetGameTime().ToString("HH:mm:ss"), aircraft);
        }

        public override void OnAircraftDamaged(int missionNumber, string shortName, AiAircraft aircraft, AiDamageInitiator initiator, NamedDamageTypes damageType)
        {
            base.OnAircraftDamaged(missionNumber, shortName, aircraft, initiator, damageType);
            _eventOutHandler.LogAircraftDamaged(GetGameTime().ToString("HH:mm:ss"), aircraft,initiator,damageType);
        }

        public override void OnAircraftCutLimb(int missionNumber, string shortName, AiAircraft aircraft, AiDamageInitiator initiator, LimbNames limbName)
        {
            base.OnAircraftCutLimb(missionNumber, shortName, aircraft, initiator, limbName);
            _eventOutHandler.LogAircraftCutLimb(GetGameTime().ToString("HH:mm:ss"), aircraft, initiator, limbName);
        }

        public override void OnAircraftLimbDamaged(int missionNumber, string shortName, AiAircraft aircraft, AiLimbDamage limbDamage)
        {
            base.OnAircraftLimbDamaged(missionNumber, shortName, aircraft, limbDamage);
            _eventOutHandler.LogAircraftLimbDamaged(GetGameTime().ToString("HH:mm:ss"), aircraft, limbDamage);
        }

        #endregion Events needed in EventOut handler not referenced elsewhere

        #region Events used in survival/scoring system
        public override void OnAircraftLanded(int missionNumber, string shortName, AiAircraft aircraft)
        {
            base.OnAircraftLanded(missionNumber, shortName, aircraft);

            if (PilotSurvivalSystemEnabled)
            {
                _pilotSurvivalSystem.AircraftLanded(aircraft);
            }
            else
            {
                _eventOutHandler.LogAircraftLanded(GetGameTime().ToString("HH:mm:ss"), aircraft, "");
            }
        }

        public override void OnAircraftCrashLanded(int missionNumber, string shortName, AiAircraft aircraft)
        {
            base.OnAircraftCrashLanded(missionNumber, shortName, aircraft);

            Timeout(300, () => DestroyActor(aircraft)); // Despawn of crash landed aircraft after 300 secs

            if (PilotSurvivalSystemEnabled)
            {
                _pilotSurvivalSystem.AircraftLanded(aircraft,true);
            }
            else
            {
                _eventOutHandler.LogAircraftLanded(GetGameTime().ToString("HH:mm:ss"), aircraft, "", true);
            }
        }

        public override void OnPersonParachuteLanded(AiPerson person)
        {
            base.OnPersonParachuteLanded(person);

            if (PilotSurvivalSystemEnabled)
            {
                _pilotSurvivalSystem.PersonParachuteLanded(person);
            } else
            {
                _eventOutHandler.LogPersonParachuteLanded(GetGameTime().ToString("HH:mm:ss"), person, "");
            }
        }

        public override void OnPersonParachuteFailed(AiPerson person)
        {
            base.OnPersonParachuteLanded(person);

            if (PilotSurvivalSystemEnabled)
            {
                _pilotSurvivalSystem.PersonParachuteFailed(person);
            } else
            {
                _eventOutHandler.LogPersonParachuteFailed(GetGameTime().ToString("HH:mm:ss"), person, "");
            }
        }

        public override void OnPersonHealth(AiPerson person, AiDamageInitiator initiator, float deltaHealth)
        {
            base.OnPersonHealth(person, initiator, deltaHealth);

            _eventOutHandler.LogPersonHealth(GetGameTime().ToString("HH:mm:ss"), person, deltaHealth);

            if (PilotSurvivalSystemEnabled)
                _pilotSurvivalSystem.OnPersonHealth(person, initiator, deltaHealth);
        }

        #endregion Events used in survival/scoring system

        #region Despawn Logic
        public override void OnActorTaskCompleted(int missionNumber, string shortName, AiActor actor)
        {
            //Message(To.Log, "OnActorTaskCompleted: " + actor.Name() + " - " + actor.IsTaskComplete()); 
            base.OnActorTaskCompleted(missionNumber, shortName, actor);

            if (DestroyAiWhenTaskComplete && IsAiActor(actor))
            {
                //Message(To.Log, "OnActorTaskCompleted: ! " + actor.Name() + " - " + actor.IsTaskComplete()); 
                DestroyActor(actor);
            }
        }

        private void DestroyFinishedGroundUnits(AiGroundGroup[] groundGroups)
        {
            if (groundGroups == null)
                return;

            foreach (AiGroundGroup groundGroup in groundGroups)
            {
                if (groundGroup.IsAlive() && groundGroup.IsTaskComplete())
                {
                   // Message(To.Log, "Destroy: " + groundGroup.Name());
                    foreach (AiActor actor in groundGroup.GetItems())
                    {
                        DestroyActor(actor);
                    }
                }
            }
        }

        /*private void DestroyFinishedAiAirGroups(AiAirGroup[] airGroups)
        {
            if (airGroups == null)
                return;

            foreach (AiAirGroup airGroup in airGroups)
            {
                if (airGroup.IsAlive() && airGroup.IsTaskComplete())
                {
                    foreach (AiActor actor in airGroup.GetItems())
                    {
                        if (IsAiActor(actor))
                        {
                            //Message(To.Log, "Destroy Air Group Actor: " + actor.Name());
                            //DestroyActor(actor);
                        }
                    }
                }
            }
        }*/

        private void DamageAiControlledPlane(AiActor actor)
        {
            if (!IsAiActor(actor))
                return;

            AiAircraft aircraft = (actor as AiAircraft);
            if (aircraft == null)
                return;

            aircraft.hitNamed(NamedDamageTypes.ControlsElevatorDisabled);
            aircraft.hitNamed(NamedDamageTypes.ControlsAileronsDisabled);
            aircraft.hitNamed(NamedDamageTypes.ControlsRudderDisabled);
            aircraft.hitNamed(NamedDamageTypes.FuelPumpFailure);

            var aiAirGroup = aircraft.Group() as AiAirGroup;
            if (aiAirGroup != null)
            {
                var iNumOfEngines = aiAirGroup.aircraftEnginesNum();
                for (var i = 0; i < iNumOfEngines; i++)
                {
                    aircraft.hitNamed((NamedDamageTypes)Enum.Parse(typeof(NamedDamageTypes), "Eng" + i.ToString(CultureInfo.InvariantCulture) + "TotalFailure"));
                }
            }
        }

        #endregion Despawn Logic


        #region Util Functions
        public object GetSectorLocation(Point3d pos)
        {
            var result = GamePlay.gpSectorName(pos.x, pos.y);
            return string.IsNullOrEmpty(result) ? string.Empty : result;
        }


        public double bearingTo(Point2d from, Point2d to)
        {
            double dx = to.x - from.x;
            double dy = to.y - from.y;
            double brng = Math.Atan2(dx, dy);
            double degree = brng * (180 / Math.PI);
            brng = (degree + 360) % 360;
            if (degree >= 0)
                return brng;
            return 360 + degree;
        }

        ///<summary>Returns the current in-game time. Note that the date is always set to 1940.07.01.</summary>
        public DateTime GetGameTime()
        {
            var dValue = GamePlay.gpTimeofDay();
            var hours = System.Math.Floor(dValue);
            var minutes = (dValue - hours) * 60.0;
            var seconds = (minutes - System.Math.Floor(minutes)) * 60.0;
            return new DateTime(1940, 7, 1, (int)System.Math.Floor(hours), (int)System.Math.Floor(minutes), (int)System.Math.Floor(seconds)); // todo Consider handling the actual date as well as time
        }

        ///<summary>Creates (spawns) a smoke plum at the given position. The kind of smoke which will be created is randomized by likelihood.</summary>
        public void SpawnRandomSmoke(Point3d pos)
        {
            string smokeType = "";
            Random random = new Random();
            int randomValue = random.Next(1, 100);

            if (randomValue <= 2)
                smokeType = "BigSitySmoke_0"; // 2% - Smoke A Big 
            else if (randomValue <= 5)
                smokeType = "Smoke2"; // 3% - Smoke B
            else if (randomValue <= 20)
                smokeType = "Smoke1"; // 15% - Smoke A
            else if (randomValue <= 40)
                smokeType = "BigSitySmoke_1"; // 20% - Smoke B Big
            else if (randomValue <= 70)
                smokeType = "BuildingFireSmall"; // 30% - Smoke Building Small
            else if (randomValue <= 100)
                smokeType = "BuildingFireBig"; // 30% - Smoke Building Big

            CultureInfo ci = Constants.Culture;
            // Generate the smoke "mission file" on the file in the RAM
            ISectionFile f = GamePlay.gpCreateSectionFile();
            f.add("Stationary", "Static0", "Smoke.Environment." + smokeType + " nn " + pos.x.ToString("F2", ci) + " " + pos.y.ToString("F2", ci) + " 0.00 /height " + pos.z.ToString("F2", ci));
            // Load the mission
            GamePlay.gpPostMissionLoad(f);
        }

        ///<summary>Returns the army which territory includes the given point (based on the front lines).</summary>
        public Army ArmyAtPos(Point3d pos)
        {
            return (Army)GamePlay.gpFrontArmy(pos.x, pos.y);
        }

        ///<summary>Returns true if the given position is within the armys home territory (based on the front lines).</summary>
        public bool IsArmyInHomeTerritory(Army army, Point3d pos)
        {
            return ArmyAtPos(pos) == army;
        }

        ///<summary>Returns true if the aircraft is within home territory (based on the aircraft position and front lines).</summary>
        public bool IsAircraftInHomeTerritroy(AiAircraft aircraft)
        {
            return IsArmyInHomeTerritory((Army)aircraft.Army(), aircraft.Pos());
        }

        ///<summary>Returns true if the person is within home territory (based on the persons position and front lines).</summary>
        public bool IsPersonInHomeTerritroy(AiPerson person)
        {
            return IsArmyInHomeTerritory((Army)person.Army(), person.Pos());
        }

        ///<summary>Returns true if the actor is controlled by AI (is not a player).</summary>
        public bool IsAiActor(AiActor actor)
        {
            bool isAi = true;
            AiCart cart = (actor as AiCart);

            if (cart != null)
            {
                for (int i = 0; i < cart.Places(); i++)
                {
                    if (cart.Player(i) != null)
                    {
                        isAi = false;
                        break;
                    }
                }
            }
            return isAi;
        }

        ///<summary>Destroys the given actor.</summary>
        public void DestroyActor(AiActor actor)
        {
            AiCart cart = (actor as AiCart);

            if (cart != null)
                cart.Destroy();
        }

        public string GenerateAircraftPlaceString(AiAircraft aircraft, int placeIndex)
        {
            return aircraft.Name() + ":" + placeIndex.ToString();
        }

        public LandTypes LandTypeAtPos(Point3d pos)
        {
            double offset = 800.0;
            double x = pos.x;
            double y = pos.y;

            LandTypes lt = GamePlay.gpLandType(x, y);

            if (lt == LandTypes.WATER)
            {
                lt = GamePlay.gpLandType(pos.x + offset, pos.y);
                if (lt == LandTypes.WATER)
                {
                    lt = GamePlay.gpLandType(pos.x - offset, pos.y);
                    if (lt == LandTypes.WATER)
                    {
                        lt = GamePlay.gpLandType(pos.x, pos.y + offset);
                        if (lt == LandTypes.WATER)
                        {
                            lt = GamePlay.gpLandType(pos.x, pos.y - offset);
                        }
                    }
                }
            }

            return lt;
        }

        public List<AiAirGroup> GetAllAirGroups()
        {
            List<AiAirGroup> result = new List<AiAirGroup>();

            foreach (Army army in Enum.GetValues(typeof(Army)))
            {
                AiAirGroup[] airGroups = GamePlay.gpAirGroups((int)army);
                if (airGroups != null)
                    result.InsertRange(0, airGroups);
            }

            return result;
        }

        public List<Tuple<AiAircraft, double>> FindAircraftInRange(Point3d pos, double range, bool justHuman = false)
        {
            List<Tuple<AiAircraft, double>> result = new List<Tuple<AiAircraft, double>>();
            List<AiAirGroup> airGroups = GetAllAirGroups();

            foreach (AiAirGroup airGroup in airGroups)
            {
                foreach (AiActor actor in airGroup.GetItems())
                {
                    AiAircraft aircraft = (actor as AiAircraft);
                    if (aircraft != null && aircraft.IsAlive() )
                    {
                        if (justHuman == true)
                        {
                            if (IsAiActor(actor) == false)
                            {
                                Point3d aircraftPos = aircraft.Pos();
                                double distance = aircraftPos.distance(ref pos);
                                if (distance <= range)
                                {
                                    result.Add(new Tuple<AiAircraft, double>(aircraft, distance));
                                }
                            }
                        }
                        else
                        {
                            Point3d aircraftPos = aircraft.Pos();
                            double distance = aircraftPos.distance(ref pos);
                            if (distance <= range)
                            {
                                result.Add(new Tuple<AiAircraft, double>(aircraft, distance));
                            }
                        }
                    }
                }
            }
            return result;
        }
        
    }

    #endregion Util Functions
}
