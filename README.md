# README #

Air Combat Group's IL-2 Cliffs of Dover Mission Script Library

http://www.aircombatgroup.co.uk/

### How do I get set up? ###

Create a envrionment variable called CLODDIR and set it to path of you CloD installation.
E.g: C:\Program Files (x86)\Steam\steamapps\common\il-2 sturmovik cliffs of dover

### Releasing a new version ###

Before releasing/publishing a new version of the ACGMission.dll increase the version and file  number in the of the assembly (Project Properties -> Application -> Assembly Information).
Commit the changes to the Git repository with a detailed commit message. The commit message should start with the new version number. 
